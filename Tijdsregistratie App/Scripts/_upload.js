﻿'use strict';
var davy = {

    disableFileInput: function(disabled) {
        if (disabled) {
            $("#getFile").attr("disabled", "disabled");
        } else {
            $("#getFile").removeAttr("disabled");
        }
    },

    getTicketnummer: function () {
        return $("#ticketnrTextBox").val();
    },

    addFile: function () {
        if ($("#getFile").val() == "") {
            alert("Je moet eerst een bestand selecteren!");
        } else {
            var file = document.getElementById("getFile");
            try {
                var key;
                var pad = $("#getFile").val().replace(/C:\\fakepath\\/i, '');
                if (pad.indexOf("\\") != -1) {
                    var bestandsnaam = pad.split("\\");
                    key = bestandsnaam[bestandsnaam.length - 1];
                } else if (pad.indexOf("/") != -1) {
                    var bestandsnaam = pad.split("/");
                    key = bestandsnaam[bestandsnaam.length - 1];
                } else {
                    key = pad
                }
                if (fileMap.contains(key)) {
                    alert(key + " is al geselecteerd!");
                } else {
                    var getFile = davy.getFileBuffer();
                    getFile.done(function (arrayBuffer) {
                        fileMap.put(key, arrayBuffer);
                        davy.updateFileList();
                    });
                    getFile.fail(function (error) {
                        davy.onError(error, "Fout bij het inlezen van " + key + ".\n")
                    });
                }
            } catch (err) {
                alert(err);
            }
        }
    },

    verwijderFile: function (file) {
        fileMap.remove(file);
        davy.updateFileList();
    },

    updateFileList: function () {
        var fileList = $("#addFileList");
        $("#fileInput").empty();
        var ul = $("<ul />");
        fileList.val("");
        fileList.empty();
        for (var i = 0; i++ < fileMap.size; fileMap.next()) {
            var key = fileMap.key();
            
            var li = $("<li />");
            var spanAttachmentFile = $("<span>" + key + "</span>")
                .attr("class", "attachmentFile");
            var spanFileListControl = $("<span />")
                .attr("class", "fileListControl");
            var upload = $("<a>Upload</a>")
                .attr("href", "#")
                .attr("onclick", "davy.uploadAttachment('" + fileMap.key() + "')");
            var verwijder = $('<a>Verwijder</a>')
                .attr("href", "#")
                .attr("onclick", "davy.verwijderFile('" + fileMap.key() + "')");
            spanFileListControl.append(verwijder);
            spanFileListControl.append(upload);
            li.append(spanAttachmentFile);
            li.append(spanFileListControl);
            ul.append(li);
        }
        fileList.append(ul);
        $("#fileInput").append(davy.getFileInput());
        davy.disableFileInput(false);
    },

    getFileInput: function () {
        return $("<input />")
        .attr("id", "getFile")
        .attr("type", "file")
        .attr("size", "50")
        .attr("onchange", "davy.addFile()")
        .attr("disabled", "disabled");
    },

    showAttachments: function (id) {
        //https://itcbe.sharepoint.com/_api/web/lists/getbytitle%28%27Ticket%27%29/items%28%2711749%27%29/AttachmentFiles
        ClearFileUpload();
        var scriptbase = hostweburl + "/_layouts/15/";
        $.getScript(scriptbase + "SP.RequestExecutor.js", function () {

            var executor = new SP.RequestExecutor(appweburl);
            var endpoint = String.format(
                    "{0}/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Ticket')" +
                    "/items('{1}')/AttachmentFiles?@target='{2}'",
                    appweburl, id, hostweburl);
            executor.executeAsync({
                url: endpoint,
                method: "GET",
                headers: {
                    "Accept": "application/json; odata=verbose"
                },
                success: function (data) {
                    var jsonObject = JSON.parse(data.body);
                    var results = jsonObject.d.results;
                    var fileList = $("#attachmentList");
                    var ul = $("<ul />");
                    for (var i = 0; i < results.length; i++) {
                        var li = $("<li />");
                        var spanAttachmentFile = $("<span />")
                            .attr("class", "attachmentFile");
                        var link = $('<a>' + results[i].FileName + '</a>')
                            .attr("href", "https://itcbe.sharepoint.com" + results[i].ServerRelativeUrl)
                            .attr("target", "_blank");
                        spanAttachmentFile.append(link);
                        var spanFileListControl = $("<span />")
                            .attr("class", "fileListControl");
                        var verwijder = $('<a>Verwijder</a>')
                            .attr("href", "#")
                            .attr("onclick", "davy.verwijderAttachment(\"" + results[i].FileName + "\")");
                        spanFileListControl.append(verwijder);
                        li.append(spanAttachmentFile);
                        li.append(spanFileListControl);
                        ul.append(li);
                        GetKlantAankondiging(klantid);
                    }
                    fileList.append(ul);
                    davy.disableFileInput(false);
                },
                error: function (error) {
                    davy.onError(error, "Fout bij het inlezen van de attachements.\n");
                }
            });
        });
    },

    //Utilities
    // Retrieve a query string value.
    // For production purposes you may want to use a library to handle the query string.
    getQueryStringParameter: function (paramToRetrieve) {
        var params = document.URL.split("?")[1].split("&");
        for (var i = 0; i < params.length; i = i + 1) {
            var singleParam = params[i].split("=");
            if (singleParam[0] == paramToRetrieve) return singleParam[1];
        }
    },

    verwijderAttachment: function (fileName) {
        var ticketnummer = davy.getTicketnummer();
        var fileCollectionEndpoint = String.format(
            "{0}/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Ticket')" +
            "/items({1})/AttachmentFiles/getbyFileName('{2}')?@target='{3}'",
            appweburl, ticketnummer, fileName, hostweburl);

        // Send the request and return the response.
        // This call returns the SharePoint site.
        return jQuery.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            headers: {
                "accept": "application/json;odata=verbose",
                "X-Http-Method": "DELETE",
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val()
            },
            success: function () {
                ClearFileUpload();
                davy.showAttachments(ticketnummer);
                davy.updateFileList();
            },
            error: function (err) {
                davy.onError(err, "Fout bij het verwijderen van bestand: " + fileName + "\n");
            }
        });
    },

    uploadAttachment: function (key) {
        var value = fileMap.get(key);
        var ticketnummer = davy.getTicketnummer();
        var addFile = addFileToFolder(key, value, ticketnummer);
        addFile.done(function (file, status, xhr) {
            fileMap.remove(key);
            ClearFileUpload();
            davy.showAttachments(ticketnummer);
            davy.updateFileList();
        });
        addFile.fail(function (error) {
            davy.onError(error, "Fout bij het opslaan van het bestand.\n");
        });
        // Add the file to the file collection in the Shared Documents folder.
        function addFileToFolder(fileName, arrayBuffer, ticketnummer) {
            // Construct the endpoint.
            var fileCollectionEndpoint = String.format(
                "{0}/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Ticket')" +
                "/items({1})/AttachmentFiles/add(FileName='{2}')?@target='{3}'",
                appweburl, ticketnummer, fileName, hostweburl);

            // Send the request and return the response.
            // This call returns the SharePoint site.
            return jQuery.ajax({
                url: fileCollectionEndpoint,
                type: "POST",
                data: arrayBuffer,
                processData: false,
                headers: {
                    "accept": "application/json;odata=verbose",
                    "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                    "content-length": arrayBuffer.byteLength
                }
            });
        }
    },

    saveAttachments: function (ticketnummer) {
        var teller = 0;
        saveAttachment(fileMap.key(), fileMap.get(fileMap.key()), ticketnummer, teller);

        function saveAttachment(key, value, ticketnummer, teller) {
            var addFile = addFileToFolder(key, value, ticketnummer);
            addFile.done(function (file, status, xhr) {
                if (teller++ < fileMap.size) {
                    alert("Bestand " + teller + ": " + key + "\n" + value);
                    fileMap.next();
                    saveAttachment(fileMap.key(), fileMap.get(fileMap.key()), ticketnummer, teller);

                } else { alert("Alles geüpload!"); }
            });
            addFile.fail(function (error) {
                davy.onError(error, "Fout bij het opslaan van het bestand.\n");
            });
        }

        // Add the file to the file collection in the Shared Documents folder.
        function addFileToFolder(fileName, arrayBuffer, ticketnummer) {
            // Construct the endpoint.
            var fileCollectionEndpoint = String.format(
                "{0}/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Ticket')" +
                "/items({1})/AttachmentFiles/add(FileName='{2}')?@target='{3}'",
                appweburl, ticketnummer, fileName, hostweburl);

            // Send the request and return the response.
            // This call returns the SharePoint site.
            return jQuery.ajax({
                url: fileCollectionEndpoint,
                type: "POST",
                data: arrayBuffer,
                processData: false,
                headers: {
                    "accept": "application/json;odata=verbose",
                    "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                    "content-length": arrayBuffer.byteLength
                }
            });
        }
    },

    getFileBuffer: function () {
        var fileInput = jQuery('#getFile');
        var deferred = jQuery.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {

            deferred.reject(e.target.error);
        }
        reader.readAsArrayBuffer(fileInput[0].files[0]);
        return deferred.promise();
    },

    //uploadFile: function () {
    //    var fileInput = jQuery('#getFile');

    //    // Initiate method calss using jQuery promise.
    //    // Get the loal file as anarray buffer.
    //    var getFile = getFileBuffer();
    //    getFile.done(function (arrayBuffer) {

    //        // Add the fiel to the SharePoint folder.
    //        var addFile = addFileToFolder(arrayBuffer);
    //        addFile.done(function (file, status, xhr) {
    //            alert('file uploaded and updated');
    //        });
    //        addFile.fail(davy.onError);
    //    });
    //    getFile.fail(davy.onError);

    //    function getFileBuffer() {
    //        var deferred = jQuery.Deferred();
    //        var reader = new FileReader();
    //        reader.onloadend = function (e) {
    //            deferred.resolve(e.target.result);
    //        }
    //        reader.onerror = function (e) {

    //            deferred.reject(e.target.error);
    //        }
    //        reader.readAsArrayBuffer(fileInput[0].files[0]);
    //        return deferred.promise();
    //    }

    //    // Add the file to the file collection in the Shared Documents folder.
    //    function addFileToFolder(arrayBuffer) {
    //        var ticketNummer = $("#ticketnrTextBox").val();
    //        // Get the file name from the file input control on the page.
    //        var parts = fileInput[0].value.split('\\');
    //        var fileName = parts[parts.length - 1];
    //        // Construct the endpoint.
    //        var fileCollectionEndpoint = String.format(
    //            "{0}/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Ticket')" +
    //            "/items({1})/AttachmentFiles/add(FileName='{2}')?@target='{3}'",
    //            appweburl, ticketNummer, fileName, hostweburl);

    //        // Send the request and return the response.
    //        // This call returns the SharePoint site.
    //        return jQuery.ajax({
    //            url: fileCollectionEndpoint,
    //            type: "POST",
    //            data: arrayBuffer,
    //            processData: false,
    //            headers: {
    //                "accept": "application/json;odata=verbose",
    //                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
    //                "content-length": arrayBuffer.byteLength
    //            }
    //        });
    //    }
    //},

    // Display error messages. 
    onError: function (error, message) {
        var errormessage = message;
        errormessage += '\n\n';
        errormessage += 'nummer: ' + JSON.parse(error.responseText).error.code;
        errormessage += '\n\n';
        errormessage += 'melding: ' + JSON.parse(error.responseText).error.message.value;
        alert(errormessage);
    }
}