﻿'use strict';
// Namespace
var be = be || {};
be.itc = be.itc || {};
be.itc.sp = be.itc.sp || {};
be.itc.sp.editor = be.itc.sp.editor || {};
be.itc.sp.editor.tinymce = be.itc.sp.editor.tinymce || {};
be.itc.sp.exception = be.itc.sp.exception || {};
be.itc.sp.rest = be.itc.sp.rest || {};
be.itc.sp.rest.fileupload = be.itc.sp.rest.fileupload || {};

var tm = be.itc.sp.editor.tinymce;
var dfd = $.Deferred();

// jQuery 1.9 is out… and $.browser has been removed – a fast workaround
jQuery.browser = {};
jQuery.browser.mozilla = /mozilla/.test(navigator.userAgent.toLowerCase()) && !/webkit/.test(navigator.userAgent.toLowerCase()) && !/trident/.test(navigator.userAgent.toLowerCase());
jQuery.browser.webkit = /webkit/.test(navigator.userAgent.toLowerCase());
jQuery.browser.opera = /opera/.test(navigator.userAgent.toLowerCase());
jQuery.browser.msie = /msie/.test(navigator.userAgent.toLowerCase());

// Define fileMap to contain files to be uploaded
var context = SP.ClientContext.get_current();
var user = context.get_web().get_currentUser();
var itcGroup;
var usercol;
var timer;
var firstTime = true;
var drempelwaardeTicket, drempelwaardeTijdsregistratie;
var SamenOpslaan = false, UpdateAll = false;
var fileMap = new Map;
var appweburl, hostweburl;
// This code runs when the DOM is ready and creates a context object which is needed to use the SharePoint object model
$(document).ready(function () {
    $("#KlantTicketsDiv").hide();
    be.itc.sp.rest.fileupload.saveEnabled = true;
    HideVorrkeurPopup();
    //$("#KlantTicketsDiv > .closeButton").hide();
    //$("#TijdsegistratieDiv > .closeButton").hide();
    be.itc.sp.editor.tinymce.setEditor();
    $("#mceu_7").css({ "width": "500" });
    getUserName();
    setDatePickers();
    SpecialTicketClick();
    appweburl = decodeURIComponent(getQueryStringParameter("SPAppWebUrl"));
    hostweburl = decodeURIComponent(getQueryStringParameter("SPHostUrl"));
    $("body").append(be.itc.sp.editor.tinymce.getNieuweBijlageDialogForm());
    $("body").append(be.itc.sp.editor.tinymce.getBestaandeBijlageDialogForm());
    $("body").append(be.itc.sp.editor.tinymce.getAfbeeldingDialogForm());
    $("#nieuweBijlageSubmit")
        .button()
        .click(function (event) {
            event.preventDefault();
            uploadBijlage();
        });
    $("#afbeeldingSubmit")
    .button()
    .click(function (event) {
        event.preventDefault();
        uploadBijlage();
    });
    $("#fileInput").append(be.itc.sp.rest.fileupload.getFileInput());
    //SetProfitableBox();
});

// This function prepares, loads, and then executes a SharePoint query to get the current users information
function getUserName() {
    context.load(user);
    context.executeQueryAsync(onGetUserNameSuccess, onGetUserNameFail);
}

// This function is executed if the above call is successful
// It replaces the contents of the 'message' element with the user name
function onGetUserNameSuccess() {
    $('#message').text(user.get_title());
    GetDrempelwaarde();
}

// This function is executed if the above call fails
function onGetUserNameFail(sender, args) {
    alert('Failed to get user name. Error:' + args.get_message());
}

function ReloadRendabel() {
    GetRegistrationsFromMonth(true)
}

function GetTickets() {
    UpdateAll = true;
    UpdateMyTickets(true);
}

function TicketRadioChange() {

    return false;
}

//Local storage filter config "Mijn tickets" bij starten van de applicatie word er uit de local storage de filteroptie gehaald als deze voorhanden zijn en toegepast.

function SaveFilterConfig() {
    var userconfig = {
        Naam: user.get_title(),
        Gepland: ($("#GeplandCheckBox").is(":checked")) == true ? "1" : "0",
        Opvolging: ($("#OpvolgCheckBox").is(":checked")) == true ? "1" : "0",
        Config: ($("#ConfigCheckBox").is(":checked")) == true ? "1" : "0",
        WachtInputKlant: ($("#WachtCheckBox").is(":checked")) == true ? "1" : "0",
        WachtOpGoedkeuring: ($("#WachtOpGoedCheckBox").is(":checked")) == true ? "1" : "0",
        WachtOpGoederen: ($("#WachtOpGoederenCheckBox").is(":checked")) == true ? "1" : "0",
        InBehandeling: ($("#InBehandelingCheckBox").is(":checked")) == true ? "1" : "0",
        OpvolgingVerlopen: ($("#OpvolgingVerlopenCheckBox").is(":checked")) == true ? "1" : "0",
        TePlannen: ($("#TePlannenCheckBox").is(":checked")) == true ? "1" : "0",
        Leveren: ($("#LeverenCheckbox").is(":checked")) == true ? "1" : "0",
        WachtExternen: ($("#WactenOpExternenCheckbox").is(":checked")) == true ? "1" : "0"
    }

    localStorage.setItem("filterconfig", JSON.stringify(userconfig));
}

function GetFilterConfig() {
    var thisUser = localStorage.getItem("filterconfig");
    var HDUser = localStorage.getItem("HDfilterconfig");

    if (thisUser !== null) {
        var config = JSON.parse(thisUser);
        // Mijn tickets
        if (config.Gepland == "1")
            $("#GeplandCheckBox").attr("checked", "checked");
        else
            $("#GeplandCheckBox").prop("checked", false);
        if (config.Opvolging == "1")
            $("#OpvolgCheckBox").attr("checked", "checked");
        else
            $("#OpvolgCheckBox").prop("checked", false);
        if (config.Config == "1")
            $("#ConfigCheckBox").attr("checked", "checked");
        else
            $("#ConfigCheckBox").prop("checked", false);
        if (config.WachtInputKlant == "1")
            $("#WachtCheckBox").attr("checked", "checked");
        else
            $("#WachtCheckBox").prop("checked", false);
        if (config.WachtOpGoedkeuring == "1")
            $("#WachtOpGoedCheckBox").attr("checked", "checked");
        else
            $("#WachtOpGoedCheckBox").prop("checked", false);
        if (config.WachtOpGoederen == "1")
            $("#WachtOpGoederenCheckBox").attr("checked", "checked");
        else
            $("#WachtOpGoederenCheckBox").prop("checked", false);
        if (config.InBehandeling == "1")
            $("#InBehandelingCheckBox").attr("checked", "checked");
        else
            $("#InBehandelingCheckBox").prop("checked", false);
        if (config.OpvolgingVerlopen == "1")
            $("#OpvolgingVerlopenCheckBox").attr("checked", "checked");
        else
            $("#OpvolgingVerlopenCheckBox").attr("checked", false);
        if (config.TePlannen == "1")
            $("#TePlannenCheckBox").attr("checked", "checked");
        else
            $("#TePlannenCheckBox").prop("checked", false);
        if (config.Leveren == "1")
            $("#LeverenCheckbox").attr("checked", "checked");
        else
            $("#LeverenCheckbox").prop("checked", false);
        if (config.WachtExternen == "1")
            $("#WactenOpExternenCheckbox").attr("checked", "checked");
        else
            $("#WactenOpExternenCheckbox").prop("checked", false);
        GetCheckedFiltersAndProcess();
    }

    if (HDUser !== null) {
        // HD tickets
        var HDconfig = JSON.parse(HDUser);
        if (HDconfig.Gepland == "1")
            $("#HelpdeskGeplandCheckbox").attr("checked", "checked");
        else
            $("#HelpdeskGeplandCheckbox").prop("checked", false);
        if (HDconfig.Opvolging == "1")
            $("#HelpeskOpvolgCheckbox").attr("checked", "checked");
        else
            $("#HelpeskOpvolgCheckbox").prop("checked", false);
        if (HDconfig.Config == "1")
            $("#HelpdeskConfigCheckbox").attr("checked", "checked");
        else
            $("#HelpdeskConfigCheckbox").prop("checked", false);
        if (HDconfig.WachtInputKlant == "1")
            $("#HelpdeskWachtCheckbox").attr("checked", "checked");
        else
            $("#HelpdeskWachtCheckbox").prop("checked", false);
        if (HDconfig.WachtOpGoedkeuring == "1")
            $("#HelpdeskWachtGoedkCheckbox").attr("checked", "checked");
        else
            $("#HelpdeskWachtGoedkCheckbox").prop("checked", false);
        if (HDconfig.WachtOpGoederen == "1")
            $("#HelpdeskWachtGoedeCheckbox").attr("checked", "checked");
        else
            $("#HelpdeskWachtGoedeCheckbox").prop("checked", false);
        if (HDconfig.InBehandeling == "1")
            $("#HelpdeskBehandelCheckbox").attr("checked", "checked");
        else
            $("#HelpdeskBehandelCheckbox").prop("checked", false);
        if (HDconfig.OpvolgingVerlopen == "1")
            $("#HelpdeskVerlopenCheckbox").attr("checked", "checked");
        else
            $("#HelpdeskVerlopenCheckbox").attr("checked", false);
        if (HDconfig.TePlannen == "1")
            $("#HelpdeskPlannenCheckbox").attr("checked", "checked");
        else
            $("#HelpdeskPlannenCheckbox").prop("checked", false);
        if (HDconfig.Leveren == "1")
            $("#HelpdeskLeverenCheckbox").attr("checked", "checked");
        else
            $("#HelpdeskLeverenCheckbox").prop("checked", false);
        if (HDconfig.WachtExternen == "1")
            $("#HelpdeskWactenOpExternenCheckbox").attr("checked", "checked");
        else
            $("#HelpdeskWactenOpExternenCheckbox").prop("checked", false);
        GetHelpdeskCheckedFiltersAndProcess();
    }
}

function SaveHDFilterConfig() {
    var userconfig = {
        Naam: user.get_title(),
        Gepland: ($("#HelpdeskGeplandCheckbox").is(":checked")) == true ? "1" : "0",
        Opvolging: ($("#HelpeskOpvolgCheckbox").is(":checked")) == true ? "1" : "0",
        Config: ($("#HelpdeskConfigCheckbox").is(":checked")) == true ? "1" : "0",
        WachtInputKlant: ($("#HelpdeskWachtCheckbox").is(":checked")) == true ? "1" : "0",
        WachtOpGoedkeuring: ($("#HelpdeskWachtGoedkCheckbox").is(":checked")) == true ? "1" : "0",
        WachtOpGoederen: ($("#HelpdeskWachtGoedeCheckbox").is(":checked")) == true ? "1" : "0",
        InBehandeling: ($("#HelpdeskBehandelCheckbox").is(":checked")) == true ? "1" : "0",
        OpvolgingVerlopen: ($("#HelpdeskVerlopenCheckbox").is(":checked")) == true ? "1" : "0",
        TePlannen: ($("#HelpdeskPlannenCheckbox").is(":checked")) == true ? "1" : "0",
        Leveren: ($("#HelpdeskLeverenCheckbox").is(":checked")) == true ? "1" : "0",
        WachtExternen: ($("#HelpdeskWactenOpExternenCheckbox").is(":checked")) == true ? "1" : "0"
    }
    localStorage.setItem("HDfilterconfig", JSON.stringify(userconfig));
}




/* Tijdsregistratie Opslaan functies */

function RegistratieOpslaan(refresh) {
    var registratie = GetRegistratieInput();
    SaveRegistration(registratie, refresh);

    return false;
}

function refresh(refresh) {
    //if (refresh)
    //    location.reload();
    //timer = window.clearInterval(timer);
}


function CheckOnsite(klant) {
    var registraties = $("#registratieTBody > tr");
    var value = true;
    registraties.each(function () {
        var $tr = $(this);
        var thisklant = $tr.find('td:nth-child(8)').text().replace(/\s/g, "");
        klant = klant.replace(/\s/g, "");
        if (thisklant === klant) {
            var onsite = $tr.find('td:nth-child(9)').text();
            if (onsite === "Ja")
                value = false;
        }
    });
    return value;
}

function ClearRegistratie() {
    $("#KlantGegevens").empty();
    $("#ticketNummerTextBox").val("");
    $("#facturatieTextBox").val("");
    $('.onsite').each(function () {
        var radio = $(this);
        if (radio.is(':checked'))
            radio.prop('checked', false);
    });
    var einduur = $("#einduurUurSelect").val();
    var eindminuut = $("#einduurMinuutSelect").val();
    $("#startuurUurSelect").val(einduur);
    $("#startuurMinuutSelect").val(eindminuut);
    var newEndHour = parseInt(einduur) + 1;
    $("#einduurUurSelect").val(newEndHour);
    $("#einduurMinuutSelect").val("00");
    $("#klantenDocSelect").val("0");
}

/* Einde Tijdsregistraties opslaan */

/* Ticket wijzigingen opslaan functies */

function TicketOpslaanClick(refresh) {
    if (isTicketValid() && be.itc.sp.rest.fileupload.saveEnabled) {
        be.itc.sp.rest.fileupload.saveEnabled = false;
        $(".SaveButton").css({ "pointer-events": "none" });
        var ticket = GetTicketInput();
        SaveTicket(ticket, refresh);
    }

    //if (refresh)
    //    location.reload();
    return false;
}

function GetTicketInput() {
    var ticket = new Ticket();
    var toegewezenaan = [];
    $('#selectedAsignedTo option').each(function (i) {
        var option = $(this);
        toegewezenaan[i] = option.text();
    });

    ticket.ticketID = $("#ticketnrTextBox").val();

    var werken = tinyMCE.get('uitgevoerdeWerkenTextBox').getContent({ format: 'raw' });
    werken = CleanText(werken);

    ticket.UitgevoerdeWerken = "<br/>" + werken;
    ticket.ToegewezenAan = toegewezenaan;
    ticket.Status = $("#statusSelect").val();
    ticket.Prioriteit = GetPrioriteit($("#prioriteitSelect").val());
    ticket.OpvolgingsDatum = ToSharePointDate($("#opvolgingsdatumTextBox").val());

    return ticket;
}

function CleanText(werken) {
    //var html = /(<([^>]+)>)/ig;
    //werken = werken.replace(html, '');
    var lt = new RegExp('&lt;', 'g');
    werken = werken.replace(gt, "&#62;");
    var gt = new RegExp('&gt;', 'g');
    werken = werken.replace(lt, "&#60;");
    var find = '&amp;';
    var re = new RegExp(find, 'g');
    werken = werken.replace(re, '&#38;');
    //var result = '<p>' + werken + '</p>';
    return werken;
}


function GetPrioriteit(prior) {
    var prioriteit = "";
    switch (prior) {
        case '1':
            prioriteit = "1 Laag";
            break;
        case '2':
            prioriteit = "2 Gemiddeld";
            break;
        case '3':
            prioriteit = "3 Hoog";
            break;
        case '4':
            prioriteit = "4 Zeer hoog";
            break;
        default:
            prioriteit = "2 Gemiddeld";
            break;
    }
    return prioriteit;
}

function TicketInBehandelingButton_Click() {
    if ($("#ticketnrTextBox").val() !== "") {
        var ticketID = $("#ticketnrTextBox").val();
        $("#statusSelect").val('In behandeling');
        var hostWebContext = new SP.AppContextSite(context, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Ticket');
        var item = list.getItemById(ticketID);
        item.set_item('Status', 'In behandeling');
        item.update();
        context.executeQueryAsync(onTicketQuerySucceeded(true), onTicketQueryFailed);
    }
}

function onTicketQuerySucceeded(refresh) {
    RemovePopup();
    if (refresh) {
        GetTickets(refresh);
    }
}

function onTicketQueryFailed() {
    alert("fout bij het inbehandeling plaatsen van het ticket!");
}

function GetToegewezenaanToSave(user) {
    var result = "";
    switch (user) {
        case "CD":
            result = "Carlo Daniels";
            break;
        case "DC":
            result = "Davy Craenen";
            break;
        case "DJ":
            result = "Davy Janssen";
            break;
        case "DB":
            result = "Dietmar Braem";
            break;
        case "FV":
            result = "Frank Vanrusselt";
            break;
        case "GV":
            result = "Glenn Vanderborght";
            break;
        case "HD":
            result = "Helpdesk ITC Belgium";
            break;
        case "IP":
            result = "Inneke Ponet";
            break;
        case "JR":
            result = "Johan Reenaers";
            break;
        case "KHD":
            result = "Koen Henderickx";
            break;
        case "KD":
            result = "Kristof Dirkx";
            break;
        case "KH":
            result = "Kristof Herten";
            break;
        case "KP":
            result = "Kristof Peters";
            break;
        case "MD":
            result = "Mario Derese";
            break;
        case "MM":
            result = "Mark Meerten";
            break;
        case "MV":
            result = "Martijn  Valkenborgh";
            break;
        case "PL":
            result = "Planning ITC";
            break;
        case "RS":
            result = "Ron Stevens";
            break;
        case "SV":
            result = "Sander Verweij";
            break;
        case "SC":
            result = "Stefan Cardinaels";
            break;
        case "SK":
            result = "Stefan Kelchtermans";
            break;
    }
    return result;
}

function GetToegewezenaanToSet(user) {
    var result = "";
    switch (user) {
        case "Carlo Daniels":
            result = "CD";
            break;
        case "Davy Craenen":
            result = "DC";
            break;
        case "Davy Janssen":
            result = "DJ";
            break;
        case "Dietmar Braem":
            result = "DB";
            break;
        case "Frank Vanrusselt":
            result = "FV";
            break;
        case "Glenn Vanderborght":
            result = "GV";
            break;
        case "Helpdesk ITC Belgium":
            result = "HD";
            break;
        case "Inneke Ponet":
            result = "IP";
            break;
        case "Johan Reenaers":
            result = "JR";
            break;
        case "Koen Henderickx":
            result = "KHD";
            break;
        case "Kristof Dirkx":
            result = "KD";
            break;
        case "Kristof Herten":
            result = "KH";
            break;
        case "Kristof Peters":
            result = "KP";
            break;
        case "Mario Derese":
            result = "MD";
            break;
        case "Mark Meerten":
            result = "MM";
            break;
        case "Martijn  Valkenborgh":
            result = "MV";
            break;
        case "Planning ITC":
            result = "PL";
            break;
        case "Ron Stevens":
            result = "RS";
            break;
        case "Sander Verweij":
            result = "SV";
            break;
        case "Stefan Cardinaels":
            result = "SC";
            break;
        case "Stefan Kelchtermans":
            result = "SK";
            break;
    }
    return result;
}

function ClearTicket() {
    $("#ticketnrTextBox").val("");
    tinyMCE.get('uitgevoerdeWerkenTextBox').setContent("");
    $("#selectedAsignedTo option").each(function () {
        var option = $(this);
        $("#toegewezenSelect").append(option);
    });
    $("#selectedAsignedTo").empty();
    SortSelect($("#toegewezenSelect > option"));
    ClearFileUpload();
}

function ClearFileUpload() {
    $("#attachmentList").empty();
    $("#fileList").empty();
    $("#addFileList").empty();
    $("#fileInput").empty();
    $("#fileInput").append(be.itc.sp.rest.fileupload.getFileInput());
}

function GetUserID(succes, error) {
    var oGroup = context.get_web().get_siteGroups();
    itcGroup = oGroup.getByName("ITC");
    usercol = itcGroup.get_users();
    context.load(usercol);

    context.executeQueryAsync(function () {
        var result = [];
        var e = usercol.getEnumerator();
        while (e.moveNext()) {
            result.push(e.get_current());
        }
        succes(result);
    },
    error);
}

function ShowTijdsregistratieDiv() {
    $("#TijdsegistratieDiv > .closeButton").removeClass('hidden');
    var myDiv = $("#TijdsegistratieDiv");
    var overlay = $("#overlaydiv");
    overlay.addClass("overlay");
    myDiv.addClass("PopUp");
    $("#closePopUp").removeClass("hidden");
    $("#closePopUp").addClass("visible");
}

function RemovePopup() {
    $("#TijdsegistratieDiv > .closeButton").addClass('hidden');
    var myDiv = $("#TijdsegistratieDiv");
    var overlay = $("#overlaydiv");
    overlay.removeClass("overlay");
    myDiv.removeClass("PopUp");
    $("#closePopUp").removeClass("visible");
    $("#closePopUp").addClass("hidden");
}

function isTicketValid() {
    var valid = true;
    var ticketnr = $("#ticketnrTextBox").val();
    var uitgevoerdewerken = tinyMCE.get('uitgevoerdeWerkenTextBox').getContent({ format: "raw" });//$("#uitgevoerdeWerkenTextBox").val();
    //var toegewezenaan = $("#toegewezenSelect").val();
    var toegewezenAan = $('#selectedAsignedTo > option');
    var opvolgdatum = $("#opvolgingsdatumTextBox").val();
    var errortekst = "<ul>";

    if (ticketnr == "") {
        errortekst += "<li>Ticket nummer niet ingevuld!</li>";
        $("#ticketnrTextBox").css("border-color", "#ff0000");
        $("#ticketNummerValedationSigne").text(" !");
        $("#ticketNummerValedationSigne").addClass("notValid")
        valid = false;
    }
    else {
        $("#ticketnrTextBox").css("border-color", "#000000");
        $("#ticketNummerValedationSigne").text(" *");
        $("#ticketNummerValedationSigne").removeClass("notValid");
    }

    if (uitgevoerdewerken == '<p><br data-mce-bogus="1"></p>' || uitgevoerdewerken == '<p></p>') {
        errortekst += "<li>Uitgevoerde werken niet ingevuld!</li>";
        $("#uitgevoerdeWerkenTextBox").css("border-color", "#ff0000");
        $("#uitgevoerdeValidationSigne").text(" !");
        $("#uitgevoerdeValidationSigne").addClass("notValid")
        uitgevoerdeValidationSigne
        valid = false;
    }
    else {
        $("#uitgevoerdeWerkenTextBox").css("border-color", "#000000");
        $("#uitgevoerdeValidationSigne").text(" *");
        $("#uitgevoerdeValidationSigne").removeClass("notValid")
    }

    if (toegewezenAan.length < 1) {
        errortekst += "<li>Toegewezen aan niet ingevuld!</li>";
        valid = false;
    }

    if (opvolgdatum == "") {
        errortekst += "<li>Opvolgdatum niet ingevuld!</li>";
        valid = false;
    }

    if (valid === false) {
        errortekst += "</ul>";
        $("#ticketErrorDiv").html(errortekst);
        $("#ticketErrorDiv").show();
    }
    else {
        $("#ticketErrorDiv").hide();
    }

    return valid;
}


/* Einde ticket functies */

/* Samen opslaan van de registratie en het ticket */

function SamenOpslaanClick() {
    var ticketvalid = isTicketValid();
    var registratievalid = isRegistratieValid();

    if (registratievalid && ticketvalid && be.itc.sp.rest.fileupload.saveEnabled) {
        SamenOpslaan = true;
        TicketOpslaanClick(false);

        //UpdateTijdsregistraties();
        if ($("#TijdsegistratieDiv").hasClass("PopUp"))
            RemovePopup();
    }
    return false;
}



function ticketclick() {

    var ticket = $('#ticketTabel tbody tr');
    ticket.click(function () {
        var $tr = $(this);
        var id = $tr.find('td:nth-child(1)').text();

        var status = $tr.find('td:nth-child(7)').text();
        var foutmelding = $tr.find('td:nth-child(4)').text();
        var opvolgdatum = NaarDagMaandJaar($tr.find('td:nth-child(12)').text());
        var klant = $tr.find('td:nth-child(10)').text();
        var klantid = $tr.find('#KlantidHidden').val();
        var prioriteit = $tr.find('td:nth-child(6)').text();

        var uitvoerders = $tr.find("#TG").val();
        var allUsers = uitvoerders.split('; ');
        var naarselect = $("#selectedAsignedTo");
        if (naarselect.find("option").length > 0) {
            MaakSelectLeeg(naarselect, $("#toegewezenSelect"));
        }
        var vanselect = $("#toegewezenSelect > option");

        for (var i = 0; i < allUsers.length - 1; i++) {
            var user = allUsers[i];
            //var option = $("<option/>").text(o).val(o);
            vanselect.each(function () {
                var option = $(this);
                if (option.text() === user) {
                    naarselect.append(option);
                    $("#toegewezenSelect option[value='" + user + "']").remove();
                }
            });
        }
        SortSelect($("#toegewezenSelect > option"));

        $("#FormklantidHidden").val(klantid);
        document.getElementById("ticketNummerTextBox").value = id;
        document.getElementById("ticketnrTextBox").value = id;
        document.getElementById("projectSelect").value = "Geen";
        document.getElementById("statusSelect").value = status;
        $("#prioriteitSelect").val(prioriteit);
        document.getElementById("opvolgingsdatumTextBox").value = opvolgdatum;
        document.getElementById("KlantGegevens").innerHTML = klant + " | " + foutmelding;

            //GetKlantAankondiging(klantid);
            be.itc.sp.rest.fileupload.showAttachments(id);
    });

    ticket.hover(
        function () {  // mouseover
            $(this).addClass('row-highlight');
        },
                function () {  // mouseout
                    $(this).removeClass('row-highlight');
                }
        );
}

function SortSelect(select) {
    select.sort(function (a, b) {
        //return $(a).text > $(b).text;
        if (a.text > b.text) {
            return 1;
        }
        else if (a.text < b.text) {
            return -1;
        }
        else {
            return 0
        }

    });
    $("#toegewezenSelect").empty().append(select);
}

function MaakSelectLeeg(fromselect, toselect) {
    fromselect.find("option").each(function () {
        var option = $(this);
        toselect.append(option);
    });
    fromselect.empty();
}

function SpecialTicketClick() {
    $("#speciaal > li").click(function () {
        var rij = $(this).text();
        var rij2 = rij.split(' ');
        var id = rij2[0];
        document.getElementById("ticketNummerTextBox").value = id;
        document.getElementById("ticketnrTextBox").value = id;
        document.getElementById("KlantGegevens").innerHTML = "ITC Belgium ticket " + id;
    });
}

function OpenTicket() {
    var e = event || window.event;
    var $tr = $(e.srcElement).closest('tr');
    var id = $tr.find('td:nth-child(1)').text();
    var ticketurl = 'https://itcbe.sharepoint.com/Lists/Ticket/Dispform.aspx?ID=' + id;// + '&RootFolder=&IsDlg=1';
    window.open(ticketurl, '_blank', 'width=988, height=800,toolbar=no, resizable=yes, location=center, scrollbars=yes, status=no');
    return false;
}

function OpenRegistratie(id) {
    var url = 'https://itcbe.sharepoint.com/Lists/Tijdsregistratie ticket/Editform.aspx?ID=' + id;// + '&RootFolder=&IsDlg=1';
    window.open(url, '_blank', 'width=988, height=800,toolbar=no, resizable=yes, location=center, scrollbars=yes, status=no');
    return false;
}

function OpenWerkbon(id) {

    var url = 'Werkbon.aspx?ID=' + id + '&SPHostUrl=' + getQueryStringParameter("SPHostUrl");
    window.open(url, '_blank', 'width=988, height=800,toolbar=no, resizable=yes, location=center, scrollbars=yes, status=no');
    return false;
}

function ShowKlantPopUp(klantid) {
    var klantticketViewModel = new KlantTicketsViewModel(klantid);
    //klantticketViewModel.loadList();
    $("#KlantTicketsDiv > .closeButton").removeClass('hidden');
    var myDiv = $("#KlantTicketsDiv");
    var overlay = $("#overlaydiv");
    overlay.addClass("overlay");
    myDiv.addClass("PopUp");
    myDiv.show();
}

function CloseKlantPopUp() {
    $("#KlantTicketsDiv > .closeButton").addClass('hidden');
    var myDiv = $("#KlantTicketsDiv");
    var overlay = $("#overlaydiv");
    overlay.removeClass("overlay");
    myDiv.removeClass("PopUp");
    myDiv.hide();
}

function setDatePickers() {
    $.datepicker.setDefaults($.datepicker.regional["nl"]);
    var date = new Date();

    var datep = $(".datepicker");
    datep.datepicker({
        'dateFormat': 'dd/mm/yy'//,
        //maxDate: new Date()
    });

    datep.each(function () {
        var $d = $(this);
        //if ($.browser.mozilla) {
        //    $d.val(GetMozillaDate(date.toLocaleDateString(), true));
        //}
        //else if (IsSafari()) {
        $d.val(ToDateString(date.toString(), true));
        //}
        //else {
        //    $d.val(GetDateString(date.toLocaleDateString(), true));
        //}
    });

    var hour = date.getHours();
    var later = date.getHours() + 1;

    $("#startuurUurSelect").val(hour < 10 ? "0" + hour : hour);
    $("#einduurUurSelect").val(later < 10 ? "0" + later : later);
}

function CopyFactText() {
    var facturatieText = $("#facturatieTextBox").val();
    facturatieText = facturatieText.replace(/\n/g, '<BR />');
    tinyMCE.get('uitgevoerdeWerkenTextBox').setContent(facturatieText);
    return false;
}


/* Filter de ticket lijst via de checkboxen of de zoek textbox */

function ZoekKeyUp() {
    $("#txtzoek").keyup(function () {
        SetAllFilterChecked();
        var zoektext = this.value;
        var tabel = $("#ticketTabel").find("tr");

        if (zoektext === "") {
            tabel.show();
        }
        else {
            tabel.hide();

            tabel.filter(function (i, v) {
                var $t = $(this);
                var row = 10;
                if ($("#nummerRadio").is(':checked'))
                    row = 1;
                else
                    row = 10;
                var control = "";
                if ($t.is(":contains('Werkgebied')"))
                    return true;
                control = $t.find('td:nth-child(' + row + ')').text();
                console.log(control);
                if (control) {
                    if (control.toLowerCase().indexOf(zoektext.toLowerCase()) >= 0)
                        return true;
                    i++;
                }
                return false;
            }).show();
        }
    });

    SetTotaalTickets();
}

function SetAllFilterChecked() {
    var checkboxes = $("#FilterTable input:checkbox")
    checkboxes.each(function () {
        $(this).prop("checked", true);
    });
}

function SetTotaalTickets() {
    var visibleTickets = $('#ticketTabel tr:visible').length;
    var totaalTickets = $('#ticketTabel tr').length;
    $("#TotaalTicketsLabel").text((visibleTickets - 1) + "/" + (totaalTickets - 1) + " tickets gevonden.");
}

function FilterOption(ischecked, optie) {
    if (ischecked)
        $("#ticketTabel tr:contains('" + optie + "')").css("display", "");
    else
        $("#ticketTabel tr:contains('" + optie + "')").css("display", "none");
    SetTotaalTickets();
}

function GetCheckedFiltersAndProcess() {
    $("#ticketDiv input:checkbox").each(function () {
        var waarde = $(this).val();
        var checked = $(this).is(":checked");
        FilterOption(checked, waarde);
    });

    SetTotaalTickets();
}

function MoveToegewezenAanRight() {
    var toegewezenAan = $("#toegewezenSelect option:selected");
    $("#selectedAsignedTo").append(toegewezenAan);
    $("#toegewezenSelect option[value='" + toegewezenAan.text() + "']").remove();
    SortSelect($("#toegewezenSelect > option"));
}

function MoveToegewezenAanLeft() {
    var toegewezenAan = $("#selectedAsignedTo option:selected");
    $("#toegewezenSelect").append(toegewezenAan);
    $("#selectedAsignedTo option[value='" + toegewezenAan.text() + "']").remove();
    SortSelect($("#toegewezenSelect > option"));
}

function HideVorrkeurPopup() {
    $("#voorkeursSorteerDiv").hide();
}

function SaveSorting() {
    var MTkolom = $("#MTKolomSelect > option:selected").val();
    var MTvolgorde = $("#MTVolgordeSelect > option:selected").val();
    var MTinstelling = {
        Kolom: MTkolom,
        Volgorde: MTvolgorde
    }

    var HDkolom = $("#HDKolomSelect > option:selected").val();
    var HDvolgorde = $("#HDVolgordeSelect > option:selected").val();
    var HDinstelling = {
        Kolom: HDkolom,
        Volgorde: HDvolgorde
    }

    localStorage.setItem("HDSorteer", JSON.stringify(HDinstelling));
    localStorage.setItem("MTSorteer", JSON.stringify(MTinstelling));
    HideVorrkeurPopup();
}

function OpenSortingDiv() {
    var MTopties = localStorage.getItem("MTSorteer");
    var HDopties = localStorage.getItem("HDSorteer");

    if (MTopties !== null) {
        var option = JSON.parse(MTopties);
        $("#MTKolomSelect").val(option.Kolom);
        $("#MTVolgordeSelect").val(option.Volgorde);
    }

    if (HDopties != null) {
        var option = JSON.parse(HDopties);
        $("#HDKollomSelect").val(option.Kolom);
        $("#HDVolgordeSelect").val(option.Volgorde);
    }

    $("#voorkeursSorteerDiv").show();
}
