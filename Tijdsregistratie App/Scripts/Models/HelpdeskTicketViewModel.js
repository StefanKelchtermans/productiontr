﻿function HelpdeskTicketViewModel() {
    var self = this;
    //self.HelpdeskTickets = ko.observableArray();

    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Ticket');

        

        var caml = "<View>"
                        + "<Query>"
                        + "<Where>"
                        + "<And><And>"
                        + "<Gt><FieldRef Name='ID' /><Value Type='Counter'>" + drempelwaardeTicket + "</Value></Gt>"
                        + "<Neq><FieldRef Name='Status' /><Value Type='Choice'>Afgesloten</Value></Neq>"
                        + "</And>"
                        + "<Eq><FieldRef Name='Toegewezen_x0020_aan' /><Value Type='Text'>Helpdesk ITC Belgium</Value></Eq>"
                        + "</And>"
                        + "</Where>"
                        + "</Query>"
                        + "<ViewFields>"
                        + "<FieldRef Name='ID' />"
                        + "<FieldRef Name='Type_x0020_ticket' />"
                        + "<FieldRef Name='Klant' />"
                        + "<FieldRef Name='Title' />"
                        + "<FieldRef Name='Contact' />"
                        + "<FieldRef Name='Prioriteit' />"
                        + "<FieldRef Name='Toegewezen_x0020_aan' />"
                        + "<FieldRef Name='Status' />"
                        + "<FieldRef Name='Opvolgingsdatum' />"
                        + "<FieldRef Name='Werkgebied' />"
                        + "<FieldRef Name='Afdeling' />"
                        + "<FieldRef Name='Modified' />"
                        + "<FieldRef Name='Editor' />"
                        + "</ViewFields>"
                        + "<OrderBy>"
                        + "<FieldRef Name='Prioriteit' />"
                        + "</OrderBy>"
                        + "<RowLimit>1000</RowLimit>"
                        + "</View>"

        var query = new SP.CamlQuery();
        query.set_viewXml(caml);

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
            Function.createDelegate(self, self._onLoadListSucceeded),
            Function.createDelegate(self, self._onLoadListFailed)
            );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var totaalTickets = 0;
        var tdtickets = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var klant = item.get_item('Klant').get_lookupValue();
            var klantid = item.get_item('Klant').get_lookupId();
            var contact = item.get_item('Contact').get_lookupValue();
            var toegewezen = item.get_item('Toegewezen_x0020_aan');
            var usertext = "";
            for (var i = 0; i < toegewezen.length; i++) {
                var userName = toegewezen[i].get_lookupValue();
                usertext = usertext + userName + '; ';
            }
            var gewijzigddoor = GetToegewezenaanToSet(item.get_item('Editor').get_lookupValue());
            var gewijzigddate = item.get_item('Modified').toString();
            var gewijzigd = ToDateString(gewijzigddate, false);
            var opvolgdate = item.get_item('Opvolgingsdatum').toString();
            var opvolg = ToDateString(opvolgdate, false);
            var prioriteit = item.get_item('Prioriteit');
            prioriteit = prioriteit.substr(0, 1);
            
            tdtickets.push(
            {
                Type: item.get_item('Type_x0020_ticket'),
                ID: item.get_item('ID'),
                Klant: klant,
                KlantId: klantid,
                Omschrijving: item.get_item('Title'),
                Contact: contact,
                Prioriteit: prioriteit,
                ToegewezenAan: usertext,
                Status: item.get_item('Status'),
                Opvolgdatum: opvolg,
                Werkgebied: item.get_item('Werkgebied'),
                Afdeling: item.get_item('Afdeling'),
                Gewijzigd: gewijzigd,
                GewijzigdDoor: gewijzigddoor
            });
            totaalTickets++;
        }
        BindHDTickets(tdtickets);
        if (firstTime) {
            var sorteerOptie = localStorage.getItem("HDSorteer");
            var index = 5;
            var volgorde = 1;
            if (sorteerOptie != null) {
                var opties = JSON.parse(sorteerOptie);
                index = getSortingField(opties.Kolom);
                volgorde = opties.Volgorde === "Ascending" ? 0 : 1;
            }
             
            $("#helpdeskTicketTable").tablesorter({
                sortList: [[index, volgorde]]
            });
            $("#helpdeskTicketTable").trigger("updateAll");
        }
        else
            $("#helpdeskTicketTable").trigger("updateAll");
        helpdeskTicketclick();
        //SetTotaalHelpdeskTickets();
        GetHelpdeskCheckedFiltersAndProcess();
        CheckVervallenHDTickets();
        if (UpdateAll)
            UpdateTijdsregistraties();
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load file list: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}


function BindHDTickets(tickets) {
    $("#HelpdeskTbody td").remove();
    $("#HelpdeskTbody tr").remove();
    if (tickets.length > 0) {
        var j = 0;
        for (var i = 0; i < tickets.length; i++) {

            var tablerow = "<tr><td style='padding:6px; margin: 3px;'>" + tickets[i].ID + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='ShowTijdsregistratieDiv();'>" + tickets[i].Type + "</a></td>";
            tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return OpenTicket(" + tickets[i].ID + ");' ><img src='../Images/pen_paper_2-512.png' alt='Open ticket' width='20' /></a></td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + tickets[i].Omschrijving + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + tickets[i].Werkgebied + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + tickets[i].Prioriteit + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + tickets[i].Status + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + tickets[i].GewijzigdDoor + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + tickets[i].Gewijzigd + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return ShowKlantPopUp("+ tickets[i].KlantId + ");'>" + tickets[i].Klant + "</a></td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + tickets[i].Contact + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + tickets[i].Opvolgdatum + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px; display:none;'><input type='hidden' id='HelpdeskToegewezenAanHidden' value='" + tickets[i].ToegewezenAan + "'/></td>";
            tablerow += "<td style='padding:6px; margin: 3px; display:none;'><input type='hidden' id='HelpdeskKlantIdHidden' value='" + tickets[i].KlantId + "'/></td></tr>";

            $("#HelpdeskTbody") .append(tablerow);
        }
    }
}

function UpdateHDTickets(first) {
    firstTime = first;
    var HDTickets = new HelpdeskTicketViewModel();
    //HDTickets.loadList();
}

function helpdeskTicketclick() {
    var ticket = $('#helpdeskTicketTable tbody tr');
    ticket.click(function () {
        var $tr = $(this);
        var id = $tr.find('td:nth-child(1)').text();

        var status = $tr.find('td:nth-child(7)').text();
        var foutmelding = $tr.find('td:nth-child(4)').text();
        var opvolgdatum = NaarDagMaandJaar($tr.find('td:nth-child(12)').text());
        var klant = $tr.find('td:nth-child(10)').text();
        var klantid = $tr.find('#HelpdeskKlantIdHidden').val();
        var prioriteit = $tr.find('td:nth-child(6)').text();

        var uitvoerders = $tr.find("#HelpdeskToegewezenAanHidden").val();
        var allUsers = uitvoerders.split('; ');
        var naarselect = $("#selectedAsignedTo");
        if (naarselect.find("option").length > 0) {
            MaakSelectLeeg(naarselect, $("#toegewezenSelect"));
        }
        var vanselect = $("#toegewezenSelect > option");

        for (var i = 0; i < allUsers.length - 1; i++) {
            var user = allUsers[i];
            //var option = $("<option/>").text(o).val(o);
            vanselect.each(function () {
                var option = $(this);
                if (option.text() === user) {
                    naarselect.append(option);
                    $("#toegewezenSelect option[value='" + user + "']").remove();
                }
            });
        }
        SortSelect($("#toegewezenSelect > option"));

        $("#FormklantidHidden").val(klantid);
        document.getElementById("ticketNummerTextBox").value = id;
        document.getElementById("ticketnrTextBox").value = id;
        document.getElementById("projectSelect").value = "Geen";
        document.getElementById("statusSelect").value = status;


        $("#prioriteitSelect").val(prioriteit);
        document.getElementById("opvolgingsdatumTextBox").value = opvolgdatum;
        document.getElementById("KlantGegevens").innerHTML = klant + " | " + foutmelding;
        //GetKlantAankondiging(klantid);
        be.itc.sp.rest.fileupload.showAttachments(id);
    });
    ticket.hover(
        function () {  // mouseover
            $(this).addClass('row-highlight');
        },
                function () {  // mouseout
                    $(this).removeClass('row-highlight');
                }
        );
}

/* Filter de ticket lijst via de checkboxen of de zoek textbox */

function HelpdeskZoekKeyUp() {
    $("#HelpdeskZoekTextBox").keyup(function () {
        SetAllHelpdeskFilterChecked();
        var zoektext = this.value;
        var tabel = $("#helpdeskTicketTable").find("tr");

        if (zoektext === "") {
            tabel.show();
        }
        else {
            tabel.hide();

            tabel.filter(function (i, v) {
                var $t = $(this);
                var row = 10;
                if ($("#HelpdeskNummerRadio").is(':checked'))
                    row = 1;
                else
                    row = 10;
                var control = "";
                if ($t.is(":contains('Werkgebied')"))
                    return true;
                control = $t.find('td:nth-child(' + row + ')').text();
                console.log(control);
                if (control) {
                    if (control.toLowerCase().indexOf(zoektext.toLowerCase()) >= 0)
                        return true;
                    i++;
                }
                return false;
            }).show();
        }
    });

    SetTotaalHelpdeskTickets();
}

function SetAllHelpdeskFilterChecked() {
    var checkboxes = $("#HelpdeskFilterTable input:checkbox")
    checkboxes.each(function () {
        $(this).prop("checked", true);
    });
}

function SetTotaalHelpdeskTickets() {
    var visibleTickets = $('#helpdeskTicketTable tr:visible').length;
    var totaalTickets = $('#helpdeskTicketTable tr').length;
    $("#TotaaHelpdeskSpan").text((visibleTickets - 1) + "/" + (totaalTickets - 1) + " tickets gevonden.");
}

function HelpdeskFilterOption(ischecked, optie) {
    if (ischecked)
        $("#helpdeskTicketTable tr:contains('" + optie + "')").css("display", "");
    else
        $("#helpdeskTicketTable tr:contains('" + optie + "')").css("display", "none");
    SetTotaalHelpdeskTickets();
}

function GetHelpdeskCheckedFiltersAndProcess() {
    $("#HelpdeskFilterTable input:checkbox").each(function () {
        var waarde = $(this).val();
        var checked = $(this).is(":checked");
        HelpdeskFilterOption(checked, waarde);
    });

    SetTotaalHelpdeskTickets();
}

function showHideHelpdeskDiv() {
    var div = $("#HelpdeskDiv");
    var link = $("#helpdeskTicketlink");
    if (div.is(":visible")) {
        div.hide();
        link.text("Toon");
    }
    else {
        div.show();
        link.text("Verberg");
    }
}

function UpdateTDTickets() {
    location.reload();
}

function CheckVervallenHDTickets() {
    var ticket = $('#helpdeskTicketTable tbody tr');
    ticket.each(function () {
        var $tr = $(this);
        var cell = $tr.find('td:nth-child(12)');
        var opvolg = StringToDate(NaarDagMaandJaar(cell.text()));
        var dVandaag = new Date();
        //var vandaag = StringToDate(GetDateString(dVandaag, true));
        if (dVandaag >= opvolg) {
            cell.css("color", "#ff0000");
        }
        else {
            cell.css("color", "#333");
        }

    });
}

