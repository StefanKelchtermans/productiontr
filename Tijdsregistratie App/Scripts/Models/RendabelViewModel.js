﻿function GetRegistrationsFromMonth(update) {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Tijdsregistratie ticket');
        var currentdate = new Date();
        var startdate = DateToScharePoint(new Date(currentdate.getFullYear(), currentdate.getMonth(), 1));
        var enddate = DateToScharePoint(currentdate);


        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
                        + "<Query>"
                        + "<Where>"
                        + "<And><And><And>"
                        + "<Geq><FieldRef Name='Startuur' /><Value Type='DateTime'>" + startdate + "</Value></Geq>"
                        + "<Leq><FieldRef Name='Einduur' /><Value Type='DateTime'>" + enddate + "</Value></Leq>"
                        + "</And>"
                        + "<Gt><FieldRef Name='ID' /><Value Type='Counter'>" + drempelwaardeTijdsregistratie + "</Value></Gt>"
                        + "</And>"
                        + "<Eq><FieldRef Name='Author' /><Value Type='Text'><UserID/></Value></Eq>"
                        + "</And>"
                        + "</Where>"
                        + "<OrderBy>"
                        + "<FieldRef Name='Startuur' />"
                        + "</OrderBy>"
                        + "</Query>"
                        + "<ViewFields>"
                        + "<FieldRef Name='ID' />"
                        + "<FieldRef Name='Title' />"
                        + "<FieldRef Name='Klant' />"
                        + "<FieldRef Name='Startuur' />"
                        + "<FieldRef Name='Einduur' />"
                        + "<FieldRef Name='Duur' />"
                        + "<FieldRef Name='Opm_x0020_facturatie' />"
                        + "<FieldRef Name='Korte_x0020_omschrijving' />"
                        + "<FieldRef Name='Author' />"
                        + "</ViewFields>"
                        + "<RowLimit>200</RowLimit>"
                        + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var Registraties = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var startuur = ToDateString(item.get_item('Startuur').toString(), false);
            var einduur = ToDateString(item.get_item('Einduur').toString(), false);
            var regis = new Registratie();
            var klant = item.get_item('Klant');

            regis.ID = item.get_item('ID');
            regis.ticketID = item.get_item('Title');
            if (klant)
                regis.Klant = klant.get_lookupValue();

            regis.startuur = startuur;
            regis.einduur = einduur;
            regis.duur = item.get_item('Duur');
            regis.facturatieTekst = item.get_item('Korte_x0020_omschrijving');
            Registraties.push(regis);

        }
        SetProfitableBox(Registraties);
        if (update == false)
            GetTickets();
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load file list: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}


function SetProfitableBox(Registraties) {
    var currentdate = new Date();
    var strdate = "01-" + currentdate.getMonth() + "-" + currentdate.getFullYear();
    var day = 1;
    var startdate = new Date(currentdate.getFullYear(), currentdate.getMonth(), day);
    var daysInMonth = new Date(currentdate.getFullYear(), currentdate.getMonth() + 1, 0).getDate();
    var blocks = $(".redabelDiv");
    blocks.each(function () {
        var div = $(this);
        var dayOfWeek = startdate.getDay();
        if (day > daysInMonth) {
            div.hide();
        }
        else {
            if (dayOfWeek > 5 || dayOfWeek < 1)
                div.css("background-color", "gray");
            else {
                if (day < currentdate.getDate()) {
                    if (DayIsProfitable(startdate, Registraties))
                        div.css("background-color", "green");
                    else
                        div.css("background-color", "red");
                }
                else if (day == currentdate.getDate())
                    if (DayIsProfitable(startdate, Registraties))
                        div.css("background-color", "green");
                    else
                        div.css("background-color", "orange");
                else
                    div.css("background-color", "transparent");
            }
        }
        day += 1;
        startdate = new Date(currentdate.getFullYear(), currentdate.getMonth(), day);
    });
}

function DayIsProfitable(date, Registraties) {
    var profitable = false;
    var dagregistraties = [];
    var strdatum = dateToString(date);
    for (var i = 0; i < Registraties.length; i++) {
        if ((Registraties[i].startuur).indexOf(strdatum) > -1)
            dagregistraties.push(Registraties[i]);
    }
    
    var dagtotaal = 0;
    $.each(dagregistraties, function(index) {
        dagtotaal += parseFloat(dagregistraties[index].duur);
    });

    if (NoHoles(dagregistraties)){
        profitable = false;
    }

    if (Math.round(dagtotaal * 100) /  100 >= 8)
        profitable = true;

    return profitable;
}

function NoHoles(dagregistraties) {
    var valid = true;
    var aantal = 0;
    var totaleTime = 0;
    var errortext = "Er bevinden zich fouten in de tijdsregistratie!\n";
    for (var i = 0; i < dagregistraties.length; i++) {
        var current = dagregistraties[i];
        if (getTimeFromString(current.startuur) >= "08:00" && getTimeFromString(current.einduur) <= "17:00") {
            var next = dagregistraties[i + 1];
            if (next)
                if (current.einduur != next.startuur) {
                    aantal++;
                    errortext += "Registratie " + next.facturatieTekst + " met startuur: " + next.startuur + "sluit niet aan bij de eindtijd: " + current.einduur + " van de vorige registratie!\n\n";
                }
            totaleTime += parseFloat(current.duur);
        }
    }
    if ((Math.round(totaleTime * 100) / 100) < 8) {
        valid = false;
        errortext += "De totale duur is kleiner dan 8 uur tussen 8 en 17 uur!\n\n";
    }
    if (aantal > 1) {
        valid = false;
    }
    //if (valid == false)
    //    alert(errortext);
    
    return valid;
}

function getTimeFromString(date) {
    var tijd = date.split(" ")[1];
    var uur = tijd.split(":")[0];
    var minuut = tijd.split(":")[1];

    return uur + ":" + minuut;
}

function GetDuur(registratie) {
    var duur = 0;
    var startuur = stringToDate(registratie.startuur, true);
    var einduur = stringToDate(registratie.einduur, true);
    var achtuur = new Date(startuur.getFullYear(), startuur.getMonth(), startuur.getDate(), 8, 0);
    var vijfuur = new Date(startuur.getFullYear(), startuur.getMonth(), startuur.getDate(), 17, 0);

    if (startuur < achtuur && einduur < achtuur) {
        duur = 0
    }
    else if (startuur < achtuur && einduur > achtuur) {
        duur = (((einduur - achtuur) / 1000) / 60) / 60;
    }
    else if (startuur > vijfuur) {
        duur = 0;
    }
    else if (startuur < vijfuur && einduur > vijfuur) {
        duur = (((vijfuur - startuur) / 1000) / 60) / 60;
    }
    else duur = registratie.duur;

    return duur;
}