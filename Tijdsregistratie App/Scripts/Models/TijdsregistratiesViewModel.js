﻿function TijdsregistratiesViewModel(datum) {
    var self = this;

    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Tijdsregistratie ticket');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
                        + "<Query>"
                        + "<Where>"
                        + "<And><And>"
                        + "<Eq><FieldRef Name='Startuur' /><Value Type='DateTime'>" + datum + "</Value></Eq>"
                        + "<Gt><FieldRef Name='ID' /><Value Type='Counter'>" + drempelwaardeTijdsregistratie + "</Value></Gt>"
                        + "</And>"
                        + "<Eq><FieldRef Name='Author' /><Value Type='Text'><UserID/></Value></Eq>"
                        + "</And>"
                        + "</Where>"
                        + "<OrderBy>"
                        + "<FieldRef Name='Startuur' />"
                        + "</OrderBy>"
                        + "</Query>"
                        + "<ViewFields>"
                        + "<FieldRef Name='ID' />"
                        + "<FieldRef Name='Title' />"
                        + "<FieldRef Name='Klant' />"
                        + "<FieldRef Name='Startuur' />"
                        + "<FieldRef Name='Einduur' />"
                        + "<FieldRef Name='Duur' />"
                        + "<FieldRef Name='Onsite' />"
                        + "<FieldRef Name='Opm_x0020_facturatie' />"
                        + "<FieldRef Name='Korte_x0020_omschrijving' />"
                        + "<FieldRef Name='Author' />"
                        + "</ViewFields>"
                        + "<RowLimit>200</RowLimit>"
                        + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
            Function.createDelegate(self, self._onLoadListSucceeded),
            Function.createDelegate(self, self._onLoadListFailed)
            );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var totaleDuur = 0;
        var duurTussen8en17 = 0;
        Registraties = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var startuur = ToDateString(item.get_item('Startuur').toString(), false);
            var einduur = ToDateString(item.get_item('Einduur').toString(), false);
            var regis = new Registratie();
            var klant = item.get_item('Klant');

            regis.ID = item.get_item('ID');
            regis.ticketID = item.get_item('Title');
            if (klant)
                regis.Klant = klant.get_lookupValue();

            regis.startuur = startuur;
            regis.einduur = einduur;
            regis.duur = item.get_item('Duur');
            regis.onsite = item.get_item('Onsite');
            regis.facturatieTekst = item.get_item('Korte_x0020_omschrijving');
            Registraties.push(regis);
            totaleDuur += Math.round(item.get_item('Duur') * 100) / 100;
            duurTussen8en17 += GetDuur(regis);
        }
        BindTijdsregistraties(Registraties);
        $("#totaalDuurLabel").text(Math.round(totaleDuur * 100) / 100);
        $("#totaalDagDuurLabel").text(Math.round(duurTussen8en17 * 100) / 100);
        if (UpdateAll)
            UpdateAll = false;
        GetLastegistratie(new Date());
        GetFilterConfig();
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load file list: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}


function OpenWerkbon() {
    var e = event || window.event;
    var $tr = $(e.srcElement).closest('tr');
    var id = $tr.find('td:nth-child(1)').text();
    var werkbonUrl = 'Werkbon.aspx?ID=' + id;// + '&RootFolder=&IsDlg=1';
    window.open(werkbonUrl, '_blank', 'width=988, height=800,toolbar=no, resizable=yes, location=center, scrollbars=yes, status=no');
    return false;
}

function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

function UpdateTijdsregistraties() {
    var datum = $('#tijdsregistratieDatumTextBox').val();
    datum = ToSharePointDate(datum);
    datum = datum.substring(0, datum.length - 5);

    var update = new TijdsregistratiesViewModel(datum);
    //update.loadList();
}

function BindTijdsregistraties(registraties) {
    $("#registratieTBody td").remove();
    $("#registratieTBody tr").remove();
    var j = 0;
    for (var i = 0; i < registraties.length; i++) {
        var tablerow = "<tr><td style='padding:6px; margin: 3px;'>" + registraties[i].ID + "</td>";
        tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].ticketID + "</td>";
        tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return OpenRegistratie(" + registraties[i].ID + ");' ><img src='../Images/pen_paper_2-512.png' alt='Open ticket' width='20' /></a></td>";
        tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].startuur + "</td>";
        tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].einduur + "</td>";
        tablerow += "<td style='padding:6px; margin: 3px;'>" + Math.round(registraties[i].duur * 100) / 100 + "</td>";
        tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].facturatieTekst + "</td>";
        if (registraties[i].Klant)
            tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].Klant + "</td>";
        else
            tablerow += "<td style='padding:6px; margin: 3px;'></td>";
        tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].onsite + "</td>";
        tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return OpenWerkbon(" + registraties[i].ID + ");'><img src='../Images/print.gif' alt='Open werkbon' width='20' /></a></td></tr>";

        $("#tijdsregistratiesTable").find('tbody').append(tablerow);
    }
    $("#tijdsregistratiesTable > tbody > tr").hover(function () { $(this).addClass('row-highlight'); }, function () { $(this).removeClass('row-highlight'); });
}

//function registratieHover() {
//    var registraties = $("#registratieTBody tr");
//    registraties.hover(
//        function () {  // mouseover
//            $(this).addClass('row-highlight');
//        },
//                function () {  // mouseout
//                    $(this).removeClass('row-highlight');
//                }
//        );
//}

function GetRegistratieInput() {
    var registratie = new Registratie();
    var einduur = "";
    var startuur = ToSharePointDate($("#startDatumTextBox").val() + " " + $("#startuurUurSelect option:selected").text() + ":" + $("#startuurMinuutSelect option:selected").text() + ":00");
    if (parseInt($("#einduurUurSelect option:selected").text()) >= 0 && parseInt($("#einduurUurSelect option:selected").text()) < parseInt($("#startuurUurSelect option:selected").text())) {

        var tempdatum = StringToDate($("#startDatumTextBox").val());
        tempdatum.setDate(tempdatum.getDate() + 1);
        einduur = ToSharePointDate(dateToReverseString(tempdatum, true) + " " + $("#einduurUurSelect option:selected").text() + ":" + $("#einduurMinuutSelect option:selected").text() + ":00");
    }
    else
        einduur = ToSharePointDate($("#startDatumTextBox").val() + " " + $("#einduurUurSelect option:selected").text() + ":" + $("#einduurMinuutSelect option:selected").text() + ":00");
    var ticket = $("#ticketNummerTextBox").val();
    registratie.ticketID = ticket;
    registratie.startUur = startuur;
    registratie.eindUur = einduur;

    var factuurtext = $("#facturatieTextBox").val();
    //var find = '&';
    //var re = new RegExp(find, 'g');
    //factuurtext = factuurtext.replace(re, '&#38;');

    registratie.facturatieTekst = factuurtext;
    if (ticket > 20)
        registratie.klantid = $("#FormklantidHidden").val();
    else
        registratie.klantid = 676;

    registratie.onsite = $("input[type='radio'].onsite:checked").val();
    registratie.permanentie = $("input[type='radio'].permanentie:checked").val();
    registratie.typeWerk = $("#typeWerkSelect option:selected").text();
    registratie.project = $("#projectSelect option:selected").text();
    registratie.Klantendocumentatie = $('#klantenDocSelect').val();

    return registratie;
}

function SaveRegistration(regis, refresh) {
    var item;
    this.save = function () {
        if (isRegistratieValid()) {
            var hostWebContext = new SP.AppContextSite(context, getQueryStringParameter("SPHostUrl"));
            var list = hostWebContext.get_web().get_lists().getByTitle('Tijdsregistratie ticket');

            var itemCreateInfo = new SP.ListItemCreationInformation();
            item = list.addItem(itemCreateInfo);

            item.set_item("Title", regis.ticketID);
            item.set_item("Startuur", regis.startUur);
            item.set_item("Einduur", regis.eindUur);
            item.set_item("Klant", regis.klantid);
            item.set_item("Korte_x0020_omschrijving", regis.facturatieTekst);
            item.set_item("Onsite", regis.onsite);
            item.set_item("Permanentie", regis.permanentie);
            item.set_item("Type_x0020_werk", regis.typeWerk);
            item.set_item("Klantendocumentatie_x0020_aangev", regis.Klantendocumentatie);

            item.update();
            context.executeQueryAsync(Function.createDelegate(this, this.onQuerySucceeded), Function.createDelegate(this, this.onQueryFailed));
        }
    }

    this.onQuerySucceeded = function () {
        be.itc.sp.rest.fileupload.saveEnabled = true;
        $(".SaveButton").css({ "pointer-events": "auto" });
        ClearRegistratie();
        ClearTicket();
        alert("registratie opgeslagen.");
        if (refresh) {
            GetTickets();
        }
        if ($("#TijdsegistratieDiv").hasClass("PopUp"))
            RemovePopup();

    }

    this.onQueryFailed = function (sender, args) {
        be.itc.sp.rest.fileupload.saveEnabled = true;
        $(".SaveButton").css({ "pointer-events": "auto" });
        alert("Fout bij het opslaan." + args.get_message() + "\n" + args.get_stackTrace());
    }
    this.save();
}

function isRegistratieValid() {
    var valid = true;
    var today = new Date();
    var yesterday = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1);
    var tomorrow = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 1);
    var invoerdatum = StringToDate($("#startDatumTextBox").val());
    var achtuur = new Date(invoerdatum.getFullYear(), invoerdatum.getMonth(), invoerdatum.getDate(), 8, 0, 0, 0);
    var vijfuur = new Date(invoerdatum.getFullYear(), invoerdatum.getMonth(), invoerdatum.getDate(), 17, 0, 0, 0);
    var myStartuur = new Date((dateToString(StringToDate($("#startDatumTextBox").val())) + " " + $("#startuurUurSelect option:selected").text() + ":" + $("#startuurMinuutSelect option:selected").text() + ":00"));
    var myEinduur = new Date((dateToString(StringToDate($("#startDatumTextBox").val())) + " " + $("#einduurUurSelect option:selected").text() + ":" + $("#einduurMinuutSelect option:selected").text() + ":00"))


    var ticketnummer = $("#ticketNummerTextBox").val();
    var startuur = ToSharePointDate($("#startDatumTextBox").val() + " " + $("#startuurUurSelect option:selected").text() + ":" + $("#startuurMinuutSelect option:selected").text() + ":00");
    var einduur;
    if (myStartuur < achtuur || myStartuur > vijfuur || myEinduur < achtuur || myEinduur > vijfuur) {
        if (parseInt($("#einduurUurSelect option:selected").text()) >= 0 && parseInt($("#einduurUurSelect option:selected").text()) < parseInt($("#startuurUurSelect option:selected").text())) {
            var tempdatum = StringToDate($("#startDatumTextBox").val());
            tempdatum.setDate(tempdatum.getDate() + 1);
            einduur = ToSharePointDate(dateToReverseString(tempdatum, true) + " " + $("#einduurUurSelect option:selected").text() + ":" + $("#einduurMinuutSelect option:selected").text() + ":00");
        }
        else
            einduur = ToSharePointDate($("#startDatumTextBox").val() + " " + $("#einduurUurSelect option:selected").text() + ":" + $("#einduurMinuutSelect option:selected").text() + ":00");
    }
    else
        einduur = ToSharePointDate($("#startDatumTextBox").val() + " " + $("#einduurUurSelect option:selected").text() + ":" + $("#einduurMinuutSelect option:selected").text() + ":00");
    var startdate = new Date(startuur);
    var enddate = new Date(einduur);
    //if (enddate.getHours() < startdate.getHours()) {
    //    enddate.setDate(enddate.getDate() + 1);
    //    einduur = enddate.getDate() + "/" + (enddate.getMonth() + 1) + "/" + enddate.getFullYear() + " " + $("#einduurUurSelect").val() + $("#einduurMinuutSelect").val() + ":00";
    //}
    var omschrijving = $("#facturatieTextBox").val();
    var onsite = $('#onsiteJaRadio').is(':checked') == true ? "Ja" : "Nee";
    var permanentie = $('#permanentieJaRadio').is(':checked') == true ? "Ja" : "Nee";
    var typewerk = $("#typeWerkSelect").val();
    var valid = true;
    var errormessage = "<ul>";
    var klantgegevens = document.getElementById("KlantGegevens").textContent;
    var klant = klantgegevens.split("|")[0];
    var klantendoc = $('#klantenDocSelect').val();

    if ((typewerk === "Helpdesk") && (onsite === "Ja")) {
        alert("De helpdesk kan niet onsite werken!!\nGelieve dit na te kijken en aan te passen.");
        return false;
    }

    if (ticketnummer == "") {
        $("#ticketNummerTextBox").css("border-color", "#ff0000");
        $("#nummerValidationSigne").text(" !");
        $("#nummerValidationSigne").addClass("notValid")
        errormessage += "<li>Ticket nummer niet ingevuld!</li>";
        valid = false;
    }
    else {
        $("#ticketNummerTextBox").css("border-color", "#000000");
        $("#nummerValidationSigne").text(" *");
        $("#nummerValidationSigne").removeClass("notValid")
    }
    if ($("#startDatumTextBox").val() == "") {
        $("#startDatumTextBox").css("border-color", "#ff0000");
        $("#beginuurValidationSigne").text(" !");
        $("#beginuurValidationSigne").addClass("notValid");
        errormessage += "<li>Startdatum niet ingevuld!</li>";
        valid = false;
    }
    else {
        $("#startDatumTextBox").css("border-color", "#000000");
        $("#beginuurValidationSigne").text(" *");
        $("#beginuurValidationSigne").removeClass("notValid");
    }
    if ($("#eindDatumTextBox").val() == "") {
        $("#eindDatumTextBox").css("border-color", "#ff0000");
        $("#einduurValidationSigne").text(" !");
        $("#einduurValidationSigne").addClass("notValid");
        errormessage += "<li>Einddatum niet ingevuld!</li>";
        valid = false;
    }
    else {
        $("#eindDatumTextBox").css("border-color", "#000000");
        $("#einduurValidationSigne").text(" *");
        $("#einduurValidationSigne").removeClass("notValid");
    }

    var onsitelist = $(".onsite:checked");
    if (onsitelist.length < 1) {
        $("#OnsiteValidationSpan").text(" !");
        $("#OnsiteValidationSpan").addClass("notValid");
        errormessage += "<li>Onsite is niet aangegeven!</li>";
        valid = false;
    }
    else {
        if ($("#onsiteJaRadio").is(":checked")) {
            if (CheckOnsite(klant) === false) {
                alert("Deze klant heeft reeds een verplaatsing! Zet onsite op nee.");
                return false;
            }
        }
        $("#OnsiteValidationSpan").text(" *");
        $("#OnsiteValidationSpan").removeClass("notValid");
    }


    var mystartuur = SharePointStringToDate(startuur.split('+')[0]);
    var myeinduur = SharePointStringToDate(einduur.split('+')[0]);

    //if ((mystartuur < yesterday) || (mystartuur > tomorrow)) {
    //    $("#startDatumTextBox").css("border-color", "#ff0000");
    //    $("#beginuurValidationSigne").text(" !");
    //    $("#beginuurValidationSigne").addClass("notValid");
    //    errormessage += "<li>Je kan enkel registraties doen van gisteren en vandaag! (startuur)</li>";
    //    valid = false;
    //}
    //else {
    //    $("#startDatumTextBox").css("border-color", "#000000");
    //    $("#beginuurValidationSigne").text(" *");
    //    $("#beginuurValidationSigne").removeClass("notValid");
    //}

    //if ((myeinduur < yesterday) || (myeinduur > tomorrow)) {
    //    $("#eindDatumTextBox").css("border-color", "#ff0000");
    //    $("#einduurValidationSigne").text(" !");
    //    $("#einduurValidationSigne").addClass("notValid");
    //    errormessage += "<li>Je kan enkel registraties doen van gisteren en vandaag! (einduur)</li>";
    //    valid = false;
    //}
    //else {
    //    $("#eindDatumTextBox").css("border-color", "#000000");
    //    $("#einduurValidationSigne").text(" *");
    //    $("#einduurValidationSigne").removeClass("notValid");
    //}

    if (myeinduur < mystartuur) {
        $("#eindDatumTextBox").css("border-color", "#ff0000");
        $("#einduurValidationSigne").text(" !");
        $("#einduurValidationSigne").addClass("notValid");
        errormessage += "<li>Het einduur is kleiner dan het startuur!</li>";
        valid = false;
    }
    else {
        $("#eindDatumTextBox").css("border-color", "#000000");
        $("#einduurValidationSigne").text(" *");
        $("#einduurValidationSigne").removeClass("notValid");

    }

    if (omschrijving == "") {
        $("#facturatieTextBox").css("border-color", "#ff0000");
        $("#facturatieValidationSigne").text(" !");
        $("#facturatieValidationSigne").addClass("notValid");
        errormessage += "<li>Geen facturatie tekst ingevult!</li>";
        valid = false;
    }
    else {
        $("#facturatieTextBox").css("border-color", "#000000");
        $("#facturatieValidationSigne").text(" *");
        $("#facturatieValidationSigne").removeClass("notValid");
    }
    if (klantendoc == "0") {
        $('#klantenDocValidationSpan').text(" !");
        $('#klantenDocValidationSpan').addClass("notValid");
        valid = false;
        errormessage += "<li>Klantendocumentatie aangevuld niet aangegeven!</li>";
    }
    else {
        $('#klantenDocValidationSpan').text(" *");
        $('#klantenDocValidationSpan').removeClass("notValid");

    }


    if (valid == false) {
        errormessage += "</ul>";
        $("#formErrorLabel").html(errormessage);
        $("#formerrordiv").show();
    }
    else {
        $("#formErrorLabel").text("");
        $("#formerrordiv").hide();
    }

    return valid;
}
