﻿var CO = CO || {};
$(function () {
    CO.Configs = new CO_Configs();
})

// model object for the app's configuration
CO_Configs = function () {
    var self = this;
    self._loadComplete = 0;
    self._callbacks = []; // holds a list of callbacks during page load
    self.ensureConfigs = function (callback) {
        if (self._loadComplete)
            callback();
        else
            self._callbacks.push(callback);
    }
}
// utility routine
function getQueryStringParameter(paramToRetrieve) {
    var params =
    document.URL.split("?")[1].split("&");
    var strParams = "";
    for (var i = 0; i < params.length; i = i + 1) {
        var singleParam = params[i].split("=");
        if (singleParam[0] == paramToRetrieve) {
            var url = singleParam[1];
            url = url.replace('%3A', ':');
            url = url.replace('%2F', '/');
            url = url.replace('%2F', '/');
            url = url.replace('%2E', '.');
            url = url.replace('%2E', '.');
            return url;
        }
    }
}