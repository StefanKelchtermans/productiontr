﻿function GetTijdsregistratieByIdViewModel(id) {
    var self = this;

    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, "https://itcbe.sharepoint.com");
        var list = hostWebContext.get_web().get_lists().getByTitle('Tijdsregistratie ticket');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
                        + "<Query>"
                        + "<Where>"
                        + "<Eq><FieldRef Name='ID' /><Value Type='Counter'>" + id + "</Value></Eq>"
                        + "</Where>"
                        + "</Query>"
                        + "<ViewFields>"
                        + "<FieldRef Name='ID' />"
                        + "<FieldRef Name='Ticket_x0020_getal' />"
                        + "<FieldRef Name='Title' />"
                        + "<FieldRef Name='Startuur' />"
                        + "<FieldRef Name='Einduur' />"
                        + "<FieldRef Name='Duur' />"
                        + "<FieldRef Name='Opm_x0020_facturatie' />"
                        + "<FieldRef Name='Korte_x0020_omschrijving' />"
                        + "<FieldRef Name='Author' />"
                        + "</ViewFields>"
                        + "<RowLimit>1</RowLimit>"
                        + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
            Function.createDelegate(self, self._onLoadListSucceeded),
            Function.createDelegate(self, self._onLoadListFailed)
            );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var startuur = GetTime(item.get_item('Startuur'));
            var einduur = GetTime(item.get_item('Einduur'));
            var maker = item.get_item("Author").get_lookupValue();

            $("#ticketnummerTextBox").val(item.get_item('Ticket_x0020_getal'));
            $("#DatumTextBox").val(GetDate(new Date()));
            $("#startuurTextBox").val(startuur);
            $("#EinduurTextBox").val(einduur);
            $("#EngeneerTextBox").val(maker);
            $("#opmerkingArea").val(item.get_item('Korte_x0020_omschrijving'));//.html("test\nopm");

            GetTicketByID(item.get_item('Ticket_x0020_getal'));
        }
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load Tijdsregistratie: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function GetTicketByID(id) {
    var self = this;

    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, "https://itcbe.sharepoint.com");
        var list = hostWebContext.get_web().get_lists().getByTitle('Ticket');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
                        + "<Query>"
                        + "<Where>"
                        + "<Eq><FieldRef Name='ID' /><Value Type='Counter'>" + id + "</Value></Eq>"
                        + "</Where>"
                        + "</Query>"
                        + "<ViewFields>"
                        + "<FieldRef Name='Klant' />"
                        + "<FieldRef Name='Title' />"                        
                        + "</ViewFields>"
                        + "<RowLimit>1</RowLimit>"
                        + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
            Function.createDelegate(self, self._onLoadListSucceeded),
            Function.createDelegate(self, self._onLoadListFailed)
            );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var klantid = item.get_item("Klant").get_lookupId();

            $("#FoutmeldingTextBox").val(item.get_item('Title'));

            GetKlantByID(klantid);
        }
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load Ticket: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function GetKlantByID(id) {
    var self = this;

    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, "https://itcbe.sharepoint.com");
        var list = hostWebContext.get_web().get_lists().getByTitle('Klantenlijst');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
                        + "<Query>"
                        + "<Where>"
                        + "<Eq><FieldRef Name='ID' /><Value Type='Counter'>" + id + "</Value></Eq>"
                        + "</Where>"
                        + "</Query>"
                        + "<ViewFields>"
                        + "<FieldRef Name='Naam1' />"
                        + "<FieldRef Name='Postcode' />"
                        + "<FieldRef Name='Straat_x0020__x002b__x0020_numme' />"
                        + "<FieldRef Name='Locatie' />"                        
                        + "</ViewFields>"
                        + "<RowLimit>1</RowLimit>"
                        + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
            Function.createDelegate(self, self._onLoadListSucceeded),
            Function.createDelegate(self, self._onLoadListFailed)
            );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var adres = item.get_item('Naam1') + '\n' + item.get_item('Straat_x0020__x002b__x0020_numme') + '\n' + item.get_item('Postcode') + "  " + item.get_item('Locatie');

            $("#AdresTextBox").val(adres);
        }
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load Klant: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}



function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

function GetTime(date) {
    var time = date.getHours() + ":" + (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes()) + ":00";
    return time;
}

function GetDate(date) {
    var strDate = date.getDay() + "/" + (date.getMonth() < 10 ? "0" + date.getMonth(): date.getMonth()) + "/" + date.getFullYear();
    return strDate;
}

function PrintBon() {
    Popup($("#printData").html());
}

function Popup(data) {
    //var mywindow = window.open('', 'my div', 'height=1000,width=800');
    //mywindow.document.write('<html><head><title>Werkbon</title>');
    //mywindow.document.write('<link rel="stylesheet" href="../Content/WerkbonStyle.css" type="text/css" />');
    //mywindow.document.write('</head><body >');
    //mywindow.document.write(data);
    //mywindow.document.write('</body></html>');
    //mywindow.document.close();
    window.print();
    window.close();

    return true;
}

