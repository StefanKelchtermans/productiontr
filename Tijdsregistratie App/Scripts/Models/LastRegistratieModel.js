﻿function GetLastegistratie(date) {
    var self = this;

    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Tijdsregistratie ticket');
        var datum = DateToScharePoint(date);

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
                        + "<Query>"
                        + "<Where>"
                        + "<And><And>"
                        + "<Eq><FieldRef Name='Startuur' /><Value Type='Date'>" + datum + "</Value></Eq>"
                        + "<Gt><FieldRef Name='ID' /><Value Type='Counter'>" + drempelwaardeTijdsregistratie + "</Value></Gt>"
                        + "</And>"
                        + "<Eq><FieldRef Name='Author' /><Value Type='Text'><UserID/></Value></Eq>"
                        + "</And>"
                        + "</Where>"
                        + "<OrderBy>"
                        + "<FieldRef Name='ID'  Ascending='FALSE' />"
                        + "</OrderBy>"
                        + "</Query>"
                        + "<ViewFields>"
                        + "<FieldRef Name='ID' />"
                        + "<FieldRef Name='Title' />"
                        + "<FieldRef Name='Klant' />"
                        + "<FieldRef Name='Startuur' />"
                        + "<FieldRef Name='Einduur' />"
                        + "<FieldRef Name='Duur' />"
                        + "<FieldRef Name='Opm_x0020_facturatie' />"
                        + "<FieldRef Name='Korte_x0020_omschrijving' />"
                        + "<FieldRef Name='Author' />"
                        + "</ViewFields>"
                        + "<RowLimit>10</RowLimit>"
                        + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
            Function.createDelegate(self, self._onLoadListSucceeded),
            Function.createDelegate(self, self._onLoadListFailed)
            );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var totaleDuur = 0;
        Registraties = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var startuur = item.get_item('Startuur');
            var einduur = item.get_item('Einduur');
            var regis = new Registratie();
            var klant = item.get_item('Klant');

            regis.ID = item.get_item('ID');
            regis.ticketID = item.get_item('Title');
            if (klant)
                regis.Klant = klant.get_lookupValue();

            regis.startuur = startuur;
            regis.einduur = einduur;
            regis.duur = item.get_item('Duur');
            regis.facturatieTekst = item.get_item('Korte_x0020_omschrijving');
            Registraties.push(regis);
        }
        SetStartEnEinduur(Registraties);
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load file list: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function SetStartEnEinduur(registraties) {
    if (registraties.length > 0) {
        var startuur = registraties[0].einduur.getHours() < 10 ? "0" + registraties[0].einduur.getHours() : registraties[0].einduur.getHours();
        var startminuut = registraties[0].einduur.getMinutes() < 10 ? "0" + registraties[0].einduur.getMinutes() : registraties[0].einduur.getMinutes();
        $("#startuurUurSelect").val(startuur);
        $("#startuurMinuutSelect").val(startminuut);
        var newEndHour = parseInt(startuur) + 1;
        $("#einduurUurSelect").val(newEndHour < 10 ? "0" + newEndHour : newEndHour);
        $("#einduurMinuutSelect").val("00");
    }
}