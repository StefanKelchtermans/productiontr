﻿function GetKlantAankondiging(klantid) {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Aankondigingen klanten');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View><Query>"
              + "<Where>"
                + "<And>"
                   + "<Eq><FieldRef Name='Klant' LookupId='True' /><Value Type='Lookup'>" + klantid + "</Value></Eq>"
                   + "<Gt><FieldRef Name='Tonen_x0020_tot' /><Value Type='DateTime'><Today /></Value></Gt>"
                + "</And>"
              + "</Where>"
          + "</Query>"
          + "<ViewFields>"
              + "<FieldRef Name='ID' />"
              + "<FieldRef Name='Klant' />"
              + "<FieldRef Name='Omschrijving' />"
              + "<FieldRef Name='Tonen_x0020_tot' />"
              + "<FieldRef Name='Title' />"
              + "<FieldRef Name='Werkgebied' />"
          + "</ViewFields>"
        + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            arraylist.push(
            {
                ID: item.get_item('ID'),
                Omschrijving: item.get_item('Omschrijving'),
                Titel: item.get_item('Title'),
                Werkgebied: item.get_item('Werkgebied')
            });
        }
        BindAankondigingen(arraylist);
        dfd.resolve();
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load file list: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindAankondigingen(arraylist) {
    HideAankondigingPopup();
    $("#aankondigingContainer").empty();
    if (arraylist.length > 0) {
        for (var i = 0; i < arraylist.length; i++) {
            var block = "<div class='aankondiging'>";
            block += "<table><tbody><tr><td>Titel: </td><td>" + arraylist[i].Titel + "</td></tr>";
            block += "<tr><td></td><td>" + arraylist[i].Omschrijving + "</td></tr>";
            block += "<tr><td></td><td>" + arraylist[i].Werkgebied + "</td></tr>";
            block += "</div>";
            $("#aankondigingContainer").append(block);
        }
        $(".overflow").removeClass('hidden');
        $("#AankondigingenDiv").removeClass('hidden');
        setTimeout(function () {
            HideAankondigingPopup()
        }, 7000);
    }
}

function HideAankondigingPopup() {
    $(".overflow").addClass('hidden');
    $("#AankondigingenDiv").addClass('hidden');
}