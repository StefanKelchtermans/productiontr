﻿var TicketNummers;
var regIndex;

function KlantTicketsViewModel(klantid) {
    var self = this;

    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Ticket');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
                        + "<Query>"
                        + "<Where>"
                        + "<And><And>"
                        + "<Gt><FieldRef Name='ID' /><Value Type='Counter'>" + drempelwaardeTicket + "</Value></Gt>"
                        + "<Neq><FieldRef Name='Status' /><Value Type='Choice'>Afgesloten</Value></Neq>"
                        + "</And>"
                        + "<Eq><FieldRef Name='Klant' LookupId='True' /><Value Type='Lookup'>" + klantid + "</Value></Eq>"
                        + "</And>"
                        + "</Where>"
                        + "</Query>"
                        + "<ViewFields>"
                        + "<FieldRef Name='ID' />"
                        + "<FieldRef Name='Type_x0020_ticket' />"
                        + "<FieldRef Name='Klant' />"
                        + "<FieldRef Name='Title' />"
                        + "<FieldRef Name='Contact' />"
                        + "<FieldRef Name='Prioriteit' />"
                        + "<FieldRef Name='Toegewezen_x0020_aan' />"
                        + "<FieldRef Name='Status' />"
                        + "<FieldRef Name='Opvolgingsdatum' />"
                        + "<FieldRef Name='Werkgebied' />"
                        + "<FieldRef Name='Afdeling' />"
                        + "<FieldRef Name='Modified' />"
                        + "<FieldRef Name='Editor' />"
                        + "</ViewFields>"
                        + "<OrderBy>"
                        + "<FieldRef Name='Prioriteit' />"
                        + "</OrderBy>"
                        + "<RowLimit>1000</RowLimit>"
                        + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
            Function.createDelegate(self, self._onLoadListSucceeded),
            Function.createDelegate(self, self._onLoadListFailed)
            );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var totaalTickets = 0;
        var klanttickets = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var klant = item.get_item('Klant').get_lookupValue();
            var klantid = item.get_item('Klant').get_lookupId();
            var contact = item.get_item('Contact').get_lookupValue();
            var toegewezen = item.get_item('Toegewezen_x0020_aan');
            var usertext = "";
            for (var i = 0; i < toegewezen.length; i++) {
                var userName = toegewezen[i].get_lookupValue();
                usertext = usertext + userName + '; ';
            }
            var gewijzigddoor = GetToegewezenaanToSet(item.get_item('Editor').get_lookupValue());
            var gewijzigddate = item.get_item('Modified').toString();
            var gewijzigd = ToDateString(gewijzigddate, false);
            var opvolgdate = item.get_item('Opvolgingsdatum').toString();
            var opvolg = ToDateString(opvolgdate, false);
            var prioriteit = item.get_item('Prioriteit');
            prioriteit = prioriteit.substr(0, 1);
            klanttickets.push(
            {
                Type: item.get_item('Type_x0020_ticket'),
                ID: item.get_item('ID'),
                Klant: klant,
                KlantId: klantid,
                Omschrijving: item.get_item('Title'),
                Contact: contact,
                Prioriteit: prioriteit,
                ToegewezenAan: usertext,
                Status: item.get_item('Status'),
                Opvolgdatum: opvolg,
                Werkgebied: item.get_item('Werkgebied'),
                Afdeling: item.get_item('Afdeling'),
                Gewijzigd: gewijzigd,
                GewijzigdDoor: gewijzigddoor
            });
            totaalTickets++;
        }
        BindKlantTickets(klanttickets);
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load file list: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindKlantTickets(tickets) {
    $("#KlantTicketTbody td").remove();
    $("#KlantTicketTbody tr").remove();
    
    $("#klantLabel").text(tickets[0].Klant);
    TicketNummers = [];
    if (tickets.length > 0) {
        var j = 0;
        for (var i = 0; i < tickets.length; i++) {
            TicketNummers.push(tickets[i].ID);
            var tablerow = "<tr><td style='padding:6px; margin: 3px;'>" + tickets[i].ID + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + tickets[i].Type + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return OpenTicket(" + tickets[i].ID + ");' ><img src='../Images/pen_paper_2-512.png' alt='Open ticket' width='20' /></a></td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + tickets[i].Omschrijving + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + tickets[i].Werkgebied + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + tickets[i].Prioriteit + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + tickets[i].Status + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + tickets[i].GewijzigdDoor + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + tickets[i].Gewijzigd + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + tickets[i].Klant + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;;'>" + tickets[i].Contact + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + tickets[i].Opvolgdatum + "</td>";
            //tablerow += "<td style='padding:6px; margin: 3px; display:none;'><input type='hidden' id='TG' value='" + tickets[i].ToegewezenAan + "' /></td></tr>";

            $("#KlantTicketTbody").append(tablerow);
        }
    }
    GetKlantRegistraties(TicketNummers);
    //var tijdsregistratiesViewModel = new KlantTijdsregistratiesViewModel(ticketnummers);
    //tijdsregistratiesViewModel.loadList();
    
    var Klant = new GetKlantByID(tickets[0].KlantId);
    //Klant.loadList();
}

function GetKlantByID(klantid) {
    var self = this;

    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Klantenlijst');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
                        + "<Query>"
                        + "<Where>"
                        + "<Eq><FieldRef Name='ID' /><Value Type='Counter'>" + klantid + "</Value></Eq>"
                        + "</Where>"
                        + "</Query>"
                        + "<ViewFields>"
                        + "<FieldRef Name='Naam1' />"
                        + "<FieldRef Name='Fax' />"
                        + "<FieldRef Name='Groep' />"
                        + "<FieldRef Name='ID' />"
                        + "<FieldRef Name='Locatie' />"
                        + "<FieldRef Name='Opmerking_x0020_klant' />"
                        + "<FieldRef Name='Opmerking_x0020_locatie' />"
                        + "<FieldRef Name='Opmerkingen_x0020_ivm_x0020_fact' />"
                        + "<FieldRef Name='Postcode' />"
                        + "<FieldRef Name='Straat_x0020__x002b__x0020_numme' />"
                        + "<FieldRef Name='Telefoon' />"
                        + "<FieldRef Name='Tweede_x0020_naam' />"
                        + "<FieldRef Name='Vertegenwoordiger' />"
                        + "<FieldRef Name='Title' />"
                        + "</ViewFields>"
                        + "<RowLimit>1</RowLimit>"
                        + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
            Function.createDelegate(self, self._onLoadListSucceeded),
            Function.createDelegate(self, self._onLoadListFailed)
            );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            $("#bedrijfLabel").text(item.get_item('Naam1'));
            $("#adresLabel").text(item.get_item('Straat_x0020__x002b__x0020_numme') + " " + item.get_item('Postcode') + " " + item.get_item('Locatie'));
            $("#telefoonLabel").text(item.get_item('Telefoon'));
            $("#faxLabel").text(item.get_item('Fax'));
            $("#OpmFactuurCell").html(item.get_item('Opmerkingen_x0020_ivm_x0020_fact'));
            $("#OpmKlantCell").html(item.get_item('Opmerking_x0020_klant'));
        }


    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load file list: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();

}

function GetKlantRegistraties(nummers) {
    $("#klantTijdsregistratiesTbody td").remove();
    $("#klantTijdsregistratiesTbody tr").remove();
    regIndex = 0;
    KlantTijdsregistratiesViewModel(nummers[regIndex]);
}

function KlantTijdsregistratiesViewModel(nummer) {
    var self = this;

    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, "https://itcbe.sharepoint.com");
        var list = hostWebContext.get_web().get_lists().getByTitle('Tijdsregistratie ticket');

        var caml = "<View>"
                + "<Query>"
                + "<Where>"
                + "<And>"
                + "<Gt><FieldRef Name='ID' /><Value Type='Counter'>" + drempelwaardeTijdsregistratie + "</Value></Gt>"
                + "<Eq><FieldRef Name='Ticket_x0020_getal' /><Value Type='Text'>" + nummer + "</Value></Eq>"
                + "</And>"
                + "</Where>"
                + "</Query>"
                + "<ViewFields>"
                + "<FieldRef Name='ID' />"
                + "<FieldRef Name='Ticket_x0020_getal' />"
                + "<FieldRef Name='Title' />"
                + "<FieldRef Name='Startuur' />"
                + "<FieldRef Name='Einduur' />"
                + "<FieldRef Name='Duur' />"
                + "<FieldRef Name='Opm_x0020_facturatie' />"
                + "<FieldRef Name='Korte_x0020_omschrijving' />"
                + "<FieldRef Name='Author' />"
                + "</ViewFields>"
                + "<RowLimit>500</RowLimit>"
                + "</View>";

        var query = new SP.CamlQuery();
        query.set_viewXml(caml);

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
            Function.createDelegate(self, self._onLoadListSucceeded),
            Function.createDelegate(self, self._onLoadListFailed)
            );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var registraties = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var startuur = ToDateString(item.get_item('Startuur').toString(), false);
            var einduur = ToDateString(item.get_item('Einduur').toString(), false);
            var maker = item.get_item("Author").get_lookupValue();

            registraties.push({
                ID: item.get_item('ID'),
                Ticket: item.get_item('Title'),
                Startuur: startuur,
                Einduur: einduur,
                Duur: item.get_item('Duur'),
                Uitgevoerd_door: maker,
                Omschrijving: item.get_item('Korte_x0020_omschrijving')
            });
        }
        BindKlantRegistraties(registraties);
        regIndex++;
        if (regIndex < TicketNummers.length)
            KlantTijdsregistratiesViewModel(TicketNummers[regIndex]);
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load Tijdsregistratie: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindKlantRegistraties(registraties) {

    if (registraties.length > 0) {
        var j = 0;
        var totaalDuur = 0;
        for (var i = 0; i < registraties.length; i++) {
            totaalDuur += Math.round(registraties[i].Duur * 100) / 100;
            var tablerow = "<tr><td style='padding:6px; margin: 3px;'>" + registraties[i].ID + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].Ticket + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return OpenRegistratie(" + registraties[i].ID + ");' ><img src='../Images/pen_paper_2-512.png' alt='Open ticket' width='20' /></a></td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].Startuur + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].Einduur + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + Math.round(registraties[i].Duur * 100) / 100 + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].Uitgevoerd_door + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + registraties[i].Omschrijving + "</td>";

            $("#klantTijdsregistratiesTbody").append(tablerow);
        }
        $("#klantTijdsregistratiesTbody").append("<tr><td colspan='8' style='color: #0171C5; padding:6px; margin: 3px;'>Totale duur ticket: " + Math.round(totaalDuur * 100) / 100 + " uur.</td></tr>");
    }
}


