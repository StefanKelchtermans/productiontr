﻿function GetDagRegistraties() {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Tijdsregistratie ticket');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
                        + "<Query>"
                        + "<Where>"
                        + "<And><And>"
                        + "<Eq><FieldRef Name='Startuur' /><Value Type='DateTime'><Today /></Value></Eq>"
                        + "<Gt><FieldRef Name='ID' /><Value Type='Counter'>" + drempelwaardeTijdsregistratie + "</Value></Gt>"
                        + "</And>"
                        + "<Eq><FieldRef Name='Author' /><Value Type='Text'><UserID/></Value></Eq>"
                        + "</And>"
                        + "</Where>"
                        + "<OrderBy>"
                        + "<FieldRef Name='Klant' />"
                        + "</OrderBy>"
                        + "</Query>"
                        + "<ViewFields>"
                        + "<FieldRef Name='ID' />"
                        + "<FieldRef Name='Title' />"
                        + "<FieldRef Name='Klant' />"
                        + "<FieldRef Name='Startuur' />"
                        + "<FieldRef Name='Einduur' />"
                        + "<FieldRef Name='Duur' />"
                        + "<FieldRef Name='Onsite' />"
                        + "<FieldRef Name='Opm_x0020_facturatie' />"
                        + "<FieldRef Name='Korte_x0020_omschrijving' />"
                        + "<FieldRef Name='Author' />"
                        + "</ViewFields>"
                        + "<RowLimit>200</RowLimit>"
                        + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();

            arraylist.push(
            {
                ID: item.get_item('ID'),
                Klant: item.get_item('Klant').get_lookupValue(),
                Onsite: item.get_item('Onsite')
            });
        }
        
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load file list: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

