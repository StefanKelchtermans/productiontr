﻿function TicketsViewModel(naam) {
    var self = this;
    //self.Tickets = ko.observableArray();

    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Ticket');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
                        + "<Query>"
                        + "<Where>"
                        + "<And><And>"
                        + "<Gt><FieldRef Name='ID' /><Value Type='Counter'>" + drempelwaardeTicket + "</Value></Gt>"
                        + "<Neq><FieldRef Name='Status' /><Value Type='Choice'>Afgesloten</Value></Neq>"
                        + "</And>"
                        + "<Eq><FieldRef Name='Toegewezen_x0020_aan' /><Value Type='Text'>" + naam + "</Value></Eq>"
                        + "</And>"
                        + "</Where>"
                        + "</Query>"
                        + "<ViewFields>"
                        + "<FieldRef Name='ID' />"
                        + "<FieldRef Name='Type_x0020_ticket' />"
                        + "<FieldRef Name='Klant' />"
                        + "<FieldRef Name='Title' />"
                        + "<FieldRef Name='Contact' />"
                        + "<FieldRef Name='Prioriteit' />"
                        + "<FieldRef Name='Toegewezen_x0020_aan' />"
                        + "<FieldRef Name='Status' />"
                        + "<FieldRef Name='Opvolgingsdatum' />"
                        + "<FieldRef Name='Werkgebied' />"
                        + "<FieldRef Name='Afdeling' />"
                        + "<FieldRef Name='Modified' />"
                        + "<FieldRef Name='Editor' />"
                        + "</ViewFields>"
                        + "<OrderBy>"
                        + "<FieldRef Name='Prioriteit' />"
                        + "</OrderBy>"
                        + "<RowLimit>1000</RowLimit>"
                        + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
            Function.createDelegate(self, self._onLoadListSucceeded),
            Function.createDelegate(self, self._onLoadListFailed)
            );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var totaalTickets = 0;
        var mytickets = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var klant = item.get_item('Klant').get_lookupValue();
            var klantid = item.get_item('Klant').get_lookupId();
            var contact = item.get_item('Contact').get_lookupValue();
            var toegewezen = item.get_item('Toegewezen_x0020_aan');
            var usertext = "";
            for (var i = 0; i < toegewezen.length; i++) {
                var userName = toegewezen[i].get_lookupValue();
                usertext = usertext + userName + '; ';
            }
            var gewijzigddoor = GetToegewezenaanToSet(item.get_item('Editor').get_lookupValue());
            var gewijzigddate = item.get_item('Modified').toString();
            var gewijzigd = ToDateString(gewijzigddate, false);
            var opvolgdate = item.get_item('Opvolgingsdatum').toString();
            var opvolg = ToDateString(opvolgdate, false);
            var prioriteit = item.get_item('Prioriteit');
            prioriteit = prioriteit.substr(0, 1);

            mytickets.push(
            {
                Type: item.get_item('Type_x0020_ticket'),
                ID: item.get_item('ID'),
                Klant: klant,
                KlantId: klantid,
                Omschrijving: item.get_item('Title'),
                Contact: contact,
                Prioriteit: prioriteit,
                ToegewezenAan: usertext,
                Status: item.get_item('Status'),
                Opvolgdatum: opvolg,
                Werkgebied: item.get_item('Werkgebied'),
                Afdeling: item.get_item('Afdeling'),
                Gewijzigd: gewijzigd,            
                GewijzigdDoor: gewijzigddoor
            });
            totaalTickets++;
        }
        BindMyTickets(mytickets);
        if (firstTime) {
            var sorteerOptie = localStorage.getItem("MTSorteer");
            var index = 5;
            var volgorde = 1;
            if (sorteerOptie != null) {
                var opties = JSON.parse(sorteerOptie);
                index = getSortingField(opties.Kolom);
                volgorde = opties.Volgorde === "Ascending" ? 0 : 1;
            }
            $("#ticketTabel").tablesorter({
                sortList: [[index, volgorde]]
            });
            $("#ticketTabel").trigger("updateAll");
        }
        else
            $("#ticketTabel").trigger("updateAll");

        GetCheckedFiltersAndProcess();
        CheckVervallenTickets();
        ticketclick();
        SetTotaalTickets();
        if (UpdateAll)
            UpdateHDTickets(true);
        //var sorting = [[5, 1]];
        //$("#ticketTabel").trigger("sorton", [sorting]);
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load file list: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindMyTickets(tickets) {
    $("#ticketTBody td").remove();
    $("#ticketTBody tr").remove();
    if (tickets.length > 0) {
        var j = 0;
        for (var i = 0; i < tickets.length; i++) {

            var tablerow = "<tr><td style='padding:6px; margin: 3px;'>" + tickets[i].ID + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='ShowTijdsregistratieDiv();'>" + tickets[i].Type + "</a></td>";
            tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return OpenTicket(" + tickets[i].ID + ");' ><img src='../Images/pen_paper_2-512.png' alt='Open ticket' width='20' /></a></td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + tickets[i].Omschrijving + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + tickets[i].Werkgebied + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + tickets[i].Prioriteit + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + tickets[i].Status + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + tickets[i].GewijzigdDoor + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + tickets[i].Gewijzigd + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return ShowKlantPopUp(" + tickets[i].KlantId + ");'>" + tickets[i].Klant + "</a></td>";
            tablerow += "<td style='padding:6px; margin: 3px;;'>" + tickets[i].Contact + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'>" + tickets[i].Opvolgdatum + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px; display:none;'><input type='hidden' id='TG' value='" + tickets[i].ToegewezenAan + "' /></td>";
            tablerow += "<td style='padding:6px; margin: 3px; display:none;'><input type='hidden' id='KlantidHidden' value='" + tickets[i].KlantId + "' /></td></tr>";

            $("#ticketTBody").append(tablerow);
        }
    }
}

function UpdateMyTickets(first) {
    firstTime = first;
    var naam = $('#message').html();
    var myTickets = new TicketsViewModel(naam);
    //myTickets.loadList();
}

function showHideMijnDiv() {
    var div = $("#ticketDiv");
    var link = $("#mijnTicketsLink");
    if (div.is(":visible")) {
        div.hide();
        link.text("Toon");
    }
    else {
        div.show();
        link.text("Verberg");
    }
}

function CheckVervallenTickets() {
    var ticket = $('#ticketTabel tbody tr');
    ticket.each(function () {
        var $tr = $(this);
        var cell = $tr.find('td:nth-child(12)');
        var opvolg = StringToDate(NaarDagMaandJaar(cell.text()));
        var dVandaag = new Date();
        //var vandaag = StringToDate(GetDateString(dVandaag, true));
        if (dVandaag >= opvolg) {
            cell.css("color", "#ff0000");
        }
        else {
            cell.css("color", "#333");
        }

    });
}


function SaveTicket(ticket, refresh) {
    this.save = function () {
        var item;

        if (isTicketValid()) {
            GetUserID(
                    function (group) {
                        var toegewezenAan = "";
                        var teller = 0;
                        for (var i = 0; i < group.length; i++) {
                            for (var j = 0; j < ticket.ToegewezenAan.length; j++) {
                                if (group[i].get_title() === ticket.ToegewezenAan[j]) {
                                    console.log(group[i].get_title() + ', ' + group[i].get_id());
                                    if (teller === 0)
                                        toegewezenAan += group[i].get_id() + ';#' + group[i].get_title();
                                    else
                                        toegewezenAan += ';#' + group[i].get_id() + ';#' + group[i].get_title();
                                    teller++;
                                }
                            }
                        }
                        var hostWebContext = new SP.AppContextSite(context, getQueryStringParameter("SPHostUrl"));
                        var list = hostWebContext.get_web().get_lists().getByTitle('Ticket');
                        item = list.getItemById(ticket.ticketID);

                        item.set_item('Uitgevoerde_x0020_taken', ticket.UitgevoerdeWerken);
                        item.set_item('Status', ticket.Status);

                        item.set_item('Toegewezen_x0020_aan', toegewezenAan);
                        item.set_item('Prioriteit', ticket.Prioriteit);
                        item.set_item('Opvolgingsdatum', ticket.OpvolgingsDatum);

                        item.update();
                        context.executeQueryAsync(Function.createDelegate(this, this.onQuerySucceeded), Function.createDelegate(this, this.onQueryFailed));
                    },
                    function (sender, args) {
                        console.log(args.get_message());
                    });

        }
    }

    this.onQuerySucceeded = function () {
        be.itc.sp.rest.fileupload.TicketNumber = ticket.ticketID;
        var uploadFiles = be.itc.sp.rest.fileupload.uploadFileList();
        uploadFiles.done(function (e) {
            if (SamenOpslaan) {
                RegistratieOpslaan(true);
                SamenOpslaan = false;
            }
            else {
                be.itc.sp.rest.fileupload.saveEnabled = true;
                $(".SaveButton").css({ "pointer-events": "auto" });
                alert("Ticket opgeslagen.");
                if (refresh)
                    GetTickets();
                if ($("#TijdsegistratieDiv").hasClass("PopUp"))
                    RemovePopup();
            }
            ClearFileUpload();
            //be.itc.sp.rest.fileupload.showAttachments(be.itc.sp.rest.fileupload.TicketNumber);
        });
        uploadFiles.fail(function (error) {
            be.itc.sp.rest.fileupload.saveEnabled = true;
            $(".SaveButton").css({ "pointer-events": "auto" });
            be.itc.sp.rest.fileupload.onError(e, "Fout bij het uploaden van bijlagen voor ticket id " + be.itc.sp.rest.fileupload.TicketNumber + ".");
        });
        ClearTicket();
    }

    this.onQueryFailed = function () {
        be.itc.sp.rest.fileupload.saveEnabled = true;
        $(".SaveButton").css({ "pointer-events": "auto" });
        alert("Fout bij het opslaan! " + args.get_message() + "\n" + args.get_stackTrace());
    }

    this.save();
}

