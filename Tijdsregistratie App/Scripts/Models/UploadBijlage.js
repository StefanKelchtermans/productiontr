﻿
//function readFile (uploadControlId) {
//    if (!window.FileReader)
//        throw "The browser does not support HTML 5";
//    var def = new $.Deferred();
//    var element = document.getElementById(uploadControlId);
//    var file = element.files[0];
//    var parts = element.value.split("\\");
//    var fileName = parts[parts.length - 1];
//    var reader = new FileReader();
//    reader.onload = function (e) {
//        def.resolve(e.target.result, fileName);
//        }
//    reader.onerror = function (e) {
//        def.reject(e.target.error);
//        }
//    reader.readAsArrayBuffer(file);
//    return def.promise();

//}

//function uploadAttachment (id, listName, fileName, buffer) {
//    var url = "https://itcbe.sharepoint.com/_api/web/lists/getByTitle('" + listName + "')/items('" + id.toString() + "')/AttachmentFiles/add(FileName='" + fileName + "')";
//    return $.ajax({
//      url: url,
//      type: "POST",
//      data: buffer,
//      processData: false,
//      headers: {
//          Accept: "application/json;odata=verbose",
//          "X-RequestDigest": $("#__REQUESTDIGEST").val(),
//          "Content-Length": buffer.byteLength
//      }
//    });
//}

//function executeUploadAttachment (id, listname) {
//    readFile("bijlageInput").done(function (buffer, fileName) {
//        uploadAttachment(id, listname, fileName, buffer).done(function () {
//            alert("success");
//            }).fail(function () {
//               alert("error in uploading attachment");
//                })
//        }).fail(function (err) {
//            alert("error in reading file content");
//        });
//}

var appWebUrl = "";
var targetSiteUrl = "";
var digest = "";
var ready = false;

// extract the parameters from the page url (appweburl and hosturl
function StartUpload(){
    //var params = document.URL.split("?")[1].split("&");
    //for (var i = 0; i < params.length; i = i + 1) {
    //    var param = params[i].split("=");
    //    switch (param[0]) {
    //        case "SPAppWebUrl":
    //            appWebUrl = decodeURIComponent(param[1]);
    //            break;
    //        case "SPHostUrl":
    //            targetSiteUrl = decodeURIComponent(param[1]);
    //            break;
    //    }
    //}
    appWebUrl = getQueryStringParameter("SPAppWebUrl");
    targetSiteUrl = getQueryStringParameter("SPHostUrl");

    // store the digest value for easy use later
    digest = $("#__REQUESTDIGEST").val();

    // load the request executor script, once ready set a flag that can be used to check against later
    $.getScript(targetSiteUrl + "/_layouts/15/SP.RequestExecutor.js", function () {
        ready = true;
    });
    
}


function UploadFile() {
    // if we have not finished loading scripts then display an alert
    if (!ready) {
        alert("Oooooops... Please wait for the page to finish loading before attempting to upload a file");
        return;
    }

    // get the library name and file reference
    var docLibrary = "DocI";//$('#docLibraryNameInput').val();
    var fileInput = $('#bijlageInput');

    // if there is no document library name alert the user and return
    if (!docLibrary || docLibrary == '') {
        alert("Oooooops... It doesn't look like you entered the name of the library you would like to upload your file to");
        return;
    }

    // if we couldnt get a reference to the file input then alert the user and return
    if (!fileInput || fileInput.length != 1) {
        alert('Oooooops... An error occured processing your input.');
        return;
    }
    else if (!fileInput[0].files) {
        alert("Oooooops... It doesn't look like your browse supports uploading files in this way");
        return;
    }
    else if (fileInput[0].files.length <= 0) {
        alert("Oooooops it doesn't look like you selected a file. Please select a file and try again.");
        return;
    }

    // for each file in the list of files process the upload
    for (var i = 0; i < fileInput[0].files.length; i++) {
        var file = fileInput[0].files[i];
        ProcessUpload(file, docLibrary, '');
    }
}

function ProcessUpload(fileInput, docLibraryName, folderName) {
    var reader = new FileReader();
    reader.onload = function (result) {
        var fileName = '',
         libraryName = '',
         fileData = '';

        var byteArray = new Uint8Array(result.target.result)
        for (var i = 0; i < byteArray.byteLength; i++) {
            fileData += String.fromCharCode(byteArray[i])
        }

        // once we have the file perform the actual upload
        PerformUpload(docLibraryName, fileInput.name, folderName, fileData);

    };
    reader.readAsArrayBuffer(fileInput);
}

function PerformUpload(libraryName, fileName, folderName, fileData) {
    var url;

    // if there is no folder name then just upload to the root folder
    if (folderName == "") {
        url = appWebUrl + "/_api/SP.AppContextSite(@TargetSite)/web/lists/getByTitle(@TargetLibrary)/RootFolder/Files/add(url=@TargetFileName,overwrite='true')?" +
            "@TargetSite='" + targetSiteUrl + "'" +
            "&@TargetLibrary='" + libraryName + "'" +
            "&@TargetFileName='" + fileName + "'";
    }
    else {
        // if there is a folder name then upload into this folder
        url = appWebUrl + "/_api/SP.AppContextSite(@TargetSite)/web/lists/getByTitle(@TargetLibrary)/RootFolder/folders(@TargetFolderName)/files/add(url=@TargetFileName,overwrite='true')?" +
           "@TargetSite='" + targetSiteUrl + "'" +
           "&@TargetLibrary='" + libraryName + "'" +
           "&@TargetFolderName='" + folderName + "'" +
           "&@TargetFileName='" + fileName + "'";
    }

    // use the request executor (cross domain library) to perform the upload
    var reqExecutor = new SP.RequestExecutor(appWebUrl);
    reqExecutor.executeAsync({
        url: url,
        method: "POST",
        headers: {
            "Accept": "application/json; odata=verbose",
            "X-RequestDigest": digest
        },
        contentType: "application/json;odata=verbose",
        binaryStringRequestBody: true,
        body: fileData,
        success: function (x, y, z) {
            alert("Success! Your file was uploaded to SharePoint.");
        },
        error: function (x, y, z) {
            alert("Oooooops... it looks like something went wrong uploading your file.");
        }
    });
}



 
