﻿'use strict';
var be = be || {};
be.itc = be.itc || {};
be.itc.sp = be.itc.sp || {};
be.itc.sp.exception = be.itc.sp.exception || {};
be.itc.sp.rest = be.itc.sp.rest || {};
be.itc.sp.rest.fileupload = be.itc.sp.rest.fileupload || {};

be.itc.sp.rest.fileupload = (function (ns) {
    var _fileMap = new Map;

    ns.addFile = function (inputID) {
        if ($(inputID).val() == "") {
            alert("Je moet eerst een bestand selecteren!");
        } else {
            try {
                var key;
                var pad = $(inputID).val().replace(/C:\\fakepath\\/i, '');
                if (pad.indexOf("\\") != -1) {
                    var bestandsnaam = pad.split("\\");
                    key = bestandsnaam[bestandsnaam.length - 1];
                } else if (pad.indexOf("/") != -1) {
                    var bestandsnaam = pad.split("/");
                    key = bestandsnaam[bestandsnaam.length - 1];
                } else {
                    key = pad
                }
                if (_fileMap.contains(key)) {
                    alert(key + " is al geselecteerd!");
                } else {
                    var getFile = ns.getFileBuffer($(inputID)[0].files[0]);
                    getFile.done(function (arrayBuffer) {
                        _fileMap.put(key, arrayBuffer);
                        ns.showAttachments(ns.TicketNumber);
                        ns.updateFileList();
                    });
                    getFile.fail(function (error) {
                        be.itc.sp.exception.onError(error, "Fout bij het inlezen van " + key + ".\n")
                    });
                }
            } catch (err) {
                alert(err);
            }
        }
    };

    ns.deleteFile = function (file) {
        _fileMap.remove(file);
        ns.updateFileList();
    };

    ns.updateFileList = function () {
        ClearFileUpload();
        var fileList = $("#addFileList");
        var ul = $("<ul />");
        fileList.val("");
        fileList.empty();
        for (var i = 0; i++ < _fileMap.size; _fileMap.next()) {
            var key = _fileMap.key();
            var li = $("<li />");
            var spanAttachmentFile = $("<span>" + key + "</span>")
                .attr("class", "attachmentFile");
            var spanFileListControl = $("<span />")
                .attr("class", "fileListControl");
            var verwijder = $('<a>Verwijder</a>')
                .attr("href", "#")
                .attr("onclick", "be.itc.sp.rest.fileupload.deleteFile('" + key + "')");
            spanFileListControl.append(verwijder);
            li.append(spanAttachmentFile);
            li.append(spanFileListControl);
            ul.append(li);
        }
        fileList.append(ul);
    };

    ns.getFileBuffer = function (file) {
        var deferred = jQuery.Deferred();
        var reader = new FileReader();
        reader.onloadend = function (e) {
            deferred.resolve(e.target.result);
        }
        reader.onerror = function (e) {

            deferred.reject(e.target.error);
        }
        reader.readAsArrayBuffer(file);
        return deferred.promise();
    };

    ns.getFileInput = function () {
        return $("<input />")
        .attr("id", "getFile")
        .attr("type", "file")
        .attr("size", "50")
        .attr("onchange", "be.itc.sp.rest.fileupload.addFile('#getFile')");
    };

    ns.uploadAttachment = function (key) {
        var deferred = $.Deferred();
        var value = _fileMap.get(key);
        var addFile = addFileToFolder(key, value, ns.TicketNumber);
        addFile.done(function (file, status, xhr) {
            _fileMap.remove(key);
            deferred.resolve(file);
        });
        addFile.fail(function (error) {
            be.itc.sp.exception.onError(error, "Fout bij het opslaan van het bestand.\n");
            deferred.reject(error);
        });
        return deferred.promise();
        // Add the file to the file collection in the Shared Documents folder.
        function addFileToFolder(fileName, arrayBuffer, ticketNumber) {
            // Construct the endpoint.
            var fileCollectionEndpoint = String.format(
                    "{0}/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Ticket')" +
                    "/items({1})/AttachmentFiles/add(FileName='{2}')?@target='{3}'",
                    appweburl, ticketNumber, fileName, hostweburl);

            // Send the request and return the response.
            // This call returns the SharePoint site.
            return jQuery.ajax({
                url: fileCollectionEndpoint,
                type: "POST",
                data: arrayBuffer,
                processData: false,
                headers: {
                    "accept": "application/json;odata=verbose",
                    "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                    "content-length": arrayBuffer.byteLength
                }
            });
        }
    };

    ns.uploadFileList = function () {
        var deferred = $.Deferred();
        var spinner, size, i, j;
        spinner = $("<div id='IOSpinner' class='loadingSpinner'><div class='loadingSpinnerMessage'><h2 id='IOSpinnerTitle' ></h2><img src='../Images/loader_5.gif' alt='spinner' /></div></div>");
        $("body").append(spinner);
        i = 0;
        size = _fileMap.size;
        var files = [];
        for (j = 0; j < _fileMap.size; _fileMap.next()) {
            files[j++] = _fileMap.key();
        }
        uploadFile();
        return deferred.promise();

        function uploadFile() {
            if (i < size) {
                $("#IOSpinnerTitle").html(files[i] + " uploaden...");
                var upload = ns.uploadAttachment(files[i]);
                upload.done(function (file) {
                    i = i + 1;
                    uploadFile();
                });
                upload.fail(function (error) {
                    deferred.reject(error);
                });
            } else {
                spinner.remove();
                deferred.resolve();
            }
        }
    };

    ns.showAttachments = function (id) {
        ns.TicketNumber = id;
        ClearFileUpload();
        appweburl = appweburl.slice(-1) == "#" ? appweburl.slice(0, -1) : appweburl;
        var scriptbase = hostweburl + "/_layouts/15/";
        $.getScript(scriptbase + "SP.RequestExecutor.js", function () {

            var endpoint = String.format(
                        "{0}/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Ticket')" +
                        "/items('{1}')/AttachmentFiles?@target='{2}'",
                    appweburl, id, hostweburl);

            $.getScript(scriptbase + "SP.Runtime.js",
                function () {
                    $.getScript(scriptbase + "SP.js",
                        function () {
                            $.getScript(scriptbase + "SP.RequestExecutor.js", function () {
                                var context;
                                var factory;
                                var appContextSite;

                                context = new SP.ClientContext(appweburl);
                                factory = new SP.ProxyWebRequestExecutorFactory(appweburl);
                                context.set_webRequestExecutorFactory(factory);
                                appContextSite = new SP.AppContextSite(context, hostweburl);

                                var executor = new SP.RequestExecutor(appweburl);
                                executor.executeAsync({
                                    url: endpoint,
                                    method: "GET",
                                    headers: {
                                        "Accept": "application/json; odata=verbose"
                                    },
                                    success: function (data) {
                                        var jsonObject = JSON.parse(data.body);
                                        var results = jsonObject.d.results;
                                        var fileList = $("#attachmentList");
                                        var ul = $("<ul />");
                                        for (var i = 0; i < results.length; i++) {
                                            var li = $("<li />");
                                            var spanAttachmentFile = $("<span />")
                                                .attr("class", "attachmentFile");
                                            var link = $('<a>' + results[i].FileName + '</a>')
                                                .attr("href", "https://itcbe.sharepoint.com" + results[i].ServerRelativeUrl)
                                                .attr("target", "_blank");
                                            spanAttachmentFile.append(link);
                                            var spanFileListControl = $("<span />")
                                                .attr("class", "fileListControl");
                                            var verwijder = $('<a>Verwijder</a>')
                                                .attr("href", "#")
                                                .attr("onclick", "be.itc.sp.rest.fileupload.deleteAttachment(\"" + results[i].FileName + "\")");
                                            spanFileListControl.append(verwijder);
                                            li.append(spanAttachmentFile);
                                            li.append(spanFileListControl);
                                            ul.append(li);
                                        }
                                        fileList.append(ul);
                                    },
                                    error: function (error) {
                                        be.itc.sp.exception.onError(error, "Fout bij het inlezen van de attachments.\n");
                                    }
                                });
                            }

                            );
                        }
                    );
                }
            );
        });
    };

    ns.deleteAttachment = function (fileName) {
        var ticketNumber = $("#ticketnrTextBox").val();
        var fileCollectionEndpoint = String.format(
                "{0}/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Ticket')" +
                "/items({1})/AttachmentFiles/getbyFileName('{2}')?@target='{3}'",
            appweburl, ticketNumber, fileName, hostweburl);

        // Send the request and return the response.
        // This call returns the SharePoint site.
        return jQuery.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            headers: {
                "accept": "application/json;odata=verbose",
                "X-Http-Method": "DELETE",
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val()
            },
            success: function () {
                ClearFileUpload();
                ns.showAttachments(ticketNumber);
                ns.updateFileList();
            },
            error: function (err) {
                be.itc.sp.exception.onError(err, "Fout bij het verwijderen van bestand: " + fileName + "\n");
            }
        });
    };

    ns.saveAttachments = function (ticketNumber) {
        var teller = 0;
        saveAttachment(_fileMap.key(), _fileMap.get(_fileMap.key()), ticketNumber, teller);

        function saveAttachment(key, value, ticketNumber, teller) {
            var addFile = addFileToFolder(key, value);
            addFile.done(function (file, status, xhr) {
                if (teller++ < _fileMap.size) {
                    alert("Bestand " + teller + ": " + key + "\n" + value);
                    _fileMap.next();
                    saveAttachment(_fileMap.key(), _fileMap.get(_fileMap.key()), ticketNumber, teller);

                } else { alert("Alles geüpload!"); }
            });
            addFile.fail(function (error) {
                be.itc.sp.exception.onError(error, "Fout bij het opslaan van het bestand.\n");
            });
        }

        // Add the file to the file collection in the Shared Documents folder.
        function addFileToFolder(fileName, arrayBuffer, ticketNumber) {
            // Construct the endpoint.
            var fileCollectionEndpoint = String.format(
                "{0}/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Tickets')" +
                    "/items({1})/AttachmentFiles/add(FileName='{2}')?@target='{3}'" +
                "?@target='{3}'", appweburl, ticketNumber, fileName, hostweburl);

            // Send the request and return the response.
            // This call returns the SharePoint site.
            return jQuery.ajax({
                url: fileCollectionEndpoint,
                type: "POST",
                data: arrayBuffer,
                processData: false,
                headers: {
                    "accept": "application/json;odata=verbose",
                    "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                    "content-length": arrayBuffer.byteLength
                }
            });
        }
    };
    return ns;
})(be.itc.sp.rest.fileupload || {});
