﻿'use strict';
var be = be || {};
be.itc = be.itc || {};
be.itc.sp = be.itc.sp || {};
be.itc.sp.editor = be.itc.sp.editor || {};
be.itc.sp.editor.tinymce = be.itc.sp.editor.tinymce || {};
be.itc.sp.exception = be.itc.sp.exception || {};

be.itc.sp.editor.tinymce = (function (ns) {

    ns.setEditor = function () {

        tinymce.init({
            language: "nl",
            selector: "#uitgevoerdeWerkenTextBox",
            theme: "modern",
            width: "500",
            height: "150",
            strict_loading_mode: true,
            menubar: false,
            plugins: [
                "advlist autolink link image charmap hr ",
                "paste textcolor colorpicker textpattern"
            ],
            toolbar1: "styleselect | bold italic | nieuwebijlage bestaandebijlage | image afbeelding | forecolor ",
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            resize: "both",
            setup: function (ed) {
                // Add a custom button
                ed.addButton('nieuwebijlage', {
                    title: 'Nieuwe bijlage invoegen',
                    image: '../images/add_file.png',
                    onclick: function () {
                        var ticketnummer = $("#ticketnrTextBox").val();
                        if (ticketnummer != "") {
                            ns.showNieuweBijlageDialog(ed);
                        }
                    }
                });

                ed.addButton('bestaandebijlage', {
                    title: 'Bestaande bijlage invoegen',
                    image: '../images/add_link.png',
                    onclick: function () {
                        var ticketnummer = $("#ticketnrTextBox").val();
                        if (ticketnummer != "") {
                            ns.showBestaandeBijlageDialog(ed);
                        }
                    }
                });

                ed.addButton('afbeelding', {
                    title: 'Afbeelding invoegen',
                    image: '../images/add_image.png',
                    onclick: function () {
                        var ticketnummer = $("#ticketnrTextBox").val();
                        if (ticketnummer != "") {
                            ns.showAfbeeldingDialog(ed);
                        }
                    }
                });
            }
        });
    }

    ns.showNieuweBijlageDialog = function (editor) {
        var dialog;

        function uploadBijlage() {
            uploadAttachment(editor);
        }

        function uploadAttachment(editor) {
            var serverRelativeUrlToFolder = '/sites/develop_apps/Tickets bijlagen';
            var ticketnummer = $("#ticketnrTextBox").val();
            var getFile = getFileBuffer();

            getFile.done(function (arrayBuffer) {

                var addFile = addFileToFolder(arrayBuffer);
                addFile.done(function (file, status, xhr) {
                    editor.focus();
                    editor.execCommand('mceInsertContent', false, '<a href="' + "https://itcbe.sharepoint.com" + file.d.ServerRelativeUrl + '">' + file.d.FileName + '</a>');
                    //editor.selection.setContent('<a href="' + "https://itcbe.sharepoint.com" + file.d.ServerRelativeUrl + '">' + file.d.Name + '</a>');
                    dialog.dialog("close");
                });
                addFile.fail(function (error) {
                    be.itc.sp.exception.onError(error, "Fout bij het opslaan van het bestand.\n");
                    dialog.dialog("close");
                });
            });
            getFile.fail(function (error) {
                be.itc.sp.exception.onError(error, "Fout bij het bufferen van het bestand.\n");
                dialog.dialog("close");
            });

            function getFileBuffer() {
                var fileInput = jQuery('#getNieuweTicketBijlage');
                var deferred = jQuery.Deferred();
                var reader = new FileReader();
                reader.onloadend = function (e) {
                    deferred.resolve(e.target.result);
                }
                reader.onerror = function (e) {
                    deferred.reject(e.target.error);
                }
                reader.readAsArrayBuffer(fileInput[0].files[0]);
                return deferred.promise();
            }

            // Add the file to the file collection in the Shared Documents folder.
            function addFileToFolder(arrayBuffer) {
                var fileName = $("#getNieuweTicketBijlage").val().replace(/C:\\fakepath\\/i, '');
                if (fileName.indexOf("\\") != -1) {
                    var parts = fileName.split("\\");
                    fileName = parts[parts.length - 1];
                } else if (fileName.indexOf("/") != -1) {
                    var parts = fileName.split("/");
                    fileName = parts[parts.length - 1];
                }
                // Construct the endpoint.
                var fileCollectionEndpoint = String.format(
                    "{0}/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Ticket')" +
                    "/items({1})/AttachmentFiles/add(FileName='{2}')?@target='{3}'",
                    appweburl, ticketnummer, fileName, hostweburl);

                // Send the request and return the response.
                // This call returns the SharePoint site.
                return jQuery.ajax({
                    url: fileCollectionEndpoint,
                    type: "POST",
                    data: arrayBuffer,
                    processData: false,
                    headers: {
                        "accept": "application/json;odata=verbose",
                        "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                        "content-length": arrayBuffer.byteLength
                    }
                });
            }

            // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
            function getListItem(fileListItemUri) {

                // Construct the endpoint.
                // The list item URI uses the host web, but the cross-domain call is sent to the
                // app web and specifies the host web as the context site.
                fileListItemUri = fileListItemUri.replace(hostweburl, '{0}');
                fileListItemUri = fileListItemUri.replace('_api/Web', '_api/sp.appcontextsite(@target)/web');

                var listItemAllFieldsEndpoint = String.format(fileListItemUri + "?@target='{1}'",
                    appweburl, hostweburl);

                // Send the request and return the response.
                return jQuery.ajax({
                    url: listItemAllFieldsEndpoint,
                    type: "GET",
                    headers: { "accept": "application/json;odata=verbose" }
                });
            }
        }

        dialog = $("#nieuweBijlageDialog").dialog({
            autoOpen: false,
            height: 400,
            width: 600,
            modal: true,
            buttons: {
                "Voeg bijlage toe": uploadBijlage,
                Cancel: function () {
                    dialog.dialog("close");
                    editor.focus();
                }
            },
            close: function () {
                dialog.dialog("close");
                editor.focus();
            }
        });

        dialog.dialog("open");
    }

    ns.getNieuweBijlageDialogForm = function () {
        var div = $("<div />")
            .attr("id", "nieuweBijlageDialog");
        var omschrijving = $("<p>Kies de bijlage die je wilt invoegen in de Tickets bijlage bibliotheek</p>");
        var input = $("<input />")
            .attr("id", "getNieuweTicketBijlage")
            .attr("type", "file");
        var submit = $("<input />")
            .attr("id", "nieuweBijlageSubmit")
            .attr("type", "submit")
            .attr("tabindex", "-1")
            .attr("style", "position:absolute; top:-1000px");
        div.append(omschrijving);
        div.append(input);
        div.append(submit);
        return div;
    }

    ns.showBestaandeBijlageDialog = function (editor) {
        var dialog;
        var ul = $("#bestaandeBijlageDialog-bijlagen");
        ul.empty();
        var scriptbase = hostweburl + "/_layouts/15/";

        dialog = $("#bestaandeBijlageDialog").dialog({
            autoOpen: false,
            height: 400,
            width: 600,
            modal: true,
            buttons: {
                Cancel: function () {
                    dialog.dialog("close");
                    editor.focus();
                }
            },
            close: function () {
                dialog.dialog("close");
                editor.focus();
            }
        });

        $.getScript(scriptbase + "SP.RequestExecutor.js", function () {
            var serverRelativeUrlToFolder = '/sites/develop_apps/Tickets bijlagen';
            var bijlageMap = new Map;
            var executor = new SP.RequestExecutor(appweburl);
            var endpoint = String.format(
                    "{0}/_api/sp.appcontextsite(@target)/web/getfolderbyserverrelativeurl('{1}')" +
                    "/files?@target='{2}'",
                    appweburl, serverRelativeUrlToFolder, hostweburl);
            executor.executeAsync({
                url: endpoint,
                method: "GET",
                headers: {
                    "Accept": "application/json; odata=verbose"
                },
                success: function (data) {
                    var jsonObject = JSON.parse(data.body);
                    var results = jsonObject.d.results;
                    var i;
                    for (i = 0; i < results.length; i++) {
                        var url = results[i].ServerRelativeUrl;
                        var name = results[i].Name;
                        var li = $("<li />");
                        var link = $('<a>' + results[i].Name + '</a>')
                            .attr("href", "#")
                            .attr("id", "bb_" + i);
                        li.append(link);
                        ul.append(li);
                        var bijlageEigenschappen = new Array();
                        bijlageEigenschappen["url"] = url;
                        bijlageEigenschappen["name"] = name;

                        bijlageMap.put("bb_" + i, bijlageEigenschappen);
                        $("#bb_" + i).click(function (event) {
                            event.preventDefault();
                            addBijlageLink(editor, getTarget(event).id);
                        });
                    }

                    // browserverschillen 
                    // target is de officiële EcmaScript code
                    // srcElement is enkel IE
                    // verder nog een aanpassing voor Safari
                    function getTarget(obj) {
                        var targ;
                        var e = obj;
                        if (e.target) targ = e.target;
                        else if (e.srcElement) targ = e.srcElement;
                        if (targ.nodeType == 3) // defeat Safari bug
                            targ = targ.parentNode;
                        return targ;
                    }

                    function addBijlageLink(editor, key) {
                        editor.focus();
                        editor.execCommand('mceInsertContent', false, '<a href="' + "https://itcbe.sharepoint.com" + bijlageMap.get(key)["url"] + '">' + bijlageMap.get(key)["name"] + '</a>');
                        //editor.selection.setContent('<a href="' + "https://itcbe.sharepoint.com" + bijlageMap.get(key)["url"] + '">' + bijlageMap.get(key)["name"] + '</a>');
                        dialog.dialog("close");
                    }
                },
                error: function (error) {
                    be.itc.sp.exception.onError(error, "Fout bij het inlezen van de attachements.\n");
                }
            });
        });

        dialog.dialog("open");
    }

    ns.getBestaandeBijlageDialogForm = function () {
        var div = $("<div />")
            .attr("id", "bestaandeBijlageDialog");
        var omschrijving = $("<p>Kies de Tickets bijlage waarvan je een link wilt invoegen</p>");
        var bijlagenDiv = $("<div />")
            .css({ "width": " 590px", "height": "360px", "overflow": "auto" });;
        var bijlagen = $("<ul />")
            .attr("id", "bestaandeBijlageDialog-bijlagen");
        bijlagenDiv.append(bijlagen);
        div.append(omschrijving);
        div.append(bijlagenDiv);
        return div;
    }

    ns.showAfbeeldingDialog = function (editor) {
        var dialog;

        function uploadBijlage() {
            uploadAttachment(editor);
        }

        function uploadAttachment(editor) {
            var ticketnummer = $("#ticketnrTextBox").val();
            var getFile = getFileBuffer();

            getFile.done(function (arrayBuffer) {

                var addFile = addFileToFolder(arrayBuffer, ticketnummer);
                addFile.done(function (file, status, xhr) {
                    editor.focus();
                    //<img src="smiley.gif" alt="Smiley face" height="42" width="42">
                    editor.execCommand('mceInsertContent', false, '<img src="' + "https://itcbe.sharepoint.com" + file.d.ServerRelativeUrl + '" alt="' + file.d.Name + '">');
                    //editor.selection.setContent('<a href="' + "https://itcbe.sharepoint.com" + file.d.ServerRelativeUrl + '">' + file.d.Name + '</a>');
                    dialog.dialog("close");
                    ns.showAttachments(ticketnummer);
                });
                addFile.fail(function (error) {
                    be.itc.sp.exception.onError(error, "Fout bij het opslaan van het bestand.\n");
                    dialog.dialog("close");
                });
            });
            getFile.fail(function (error) {
                be.itc.sp.exception.onError(error, "Fout bij het bufferen van het bestand.\n");
                dialog.dialog("close");
            });

            function getFileBuffer() {
                var fileInput = jQuery('#getAfbeelding');
                var deferred = jQuery.Deferred();
                var reader = new FileReader();
                reader.onloadend = function (e) {
                    deferred.resolve(e.target.result);
                }
                reader.onerror = function (e) {
                    deferred.reject(e.target.error);
                }
                reader.readAsArrayBuffer(fileInput[0].files[0]);
                return deferred.promise();
            }

            // Add the file to the file collection in the Shared Documents folder.
            function addFileToFolder(arrayBuffer) {
                var fileName = $("#getAfbeelding").val().replace(/C:\\fakepath\\/i, '');
                if (fileName.indexOf("\\") != -1) {
                    var parts = fileName.split("\\");
                    fileName = parts[parts.length - 1];
                } else if (fileName.indexOf("/") != -1) {
                    var parts = fileName.split("/");
                    fileName = parts[parts.length - 1];
                }
                // Construct the endpoint.
                var fileCollectionEndpoint = String.format(
                    "{0}/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Ticket')" +
                    "/items({1})/AttachmentFiles/add(FileName='{2}')?@target='{3}'",
                    appweburl, ticketnummer, fileName, hostweburl);


                //"{0}/_api/sp.appcontextsite(@target)/web/getfolderbyserverrelativeurl('{1}')/files" +
                //"/add(overwrite=true, url='{2}')?@target='{3}'",
                //appweburl, serverRelativeUrlToFolder, fileName, hostweburl);

                // Send the request and return the response.
                // This call returns the SharePoint site.
                return jQuery.ajax({
                    url: fileCollectionEndpoint,
                    type: "POST",
                    data: arrayBuffer,
                    processData: false,
                    headers: {
                        "accept": "application/json;odata=verbose",
                        "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                        "content-length": arrayBuffer.byteLength
                    }
                });
            }

            // Get the list item that corresponds to the file by calling the file's ListItemAllFields property.
            function getListItem(fileListItemUri) {

                // Construct the endpoint.
                // The list item URI uses the host web, but the cross-domain call is sent to the
                // app web and specifies the host web as the context site.
                fileListItemUri = fileListItemUri.replace(hostweburl, '{0}');
                fileListItemUri = fileListItemUri.replace('_api/Web', '_api/sp.appcontextsite(@target)/web');

                var listItemAllFieldsEndpoint = String.format(fileListItemUri + "?@target='{1}'",
                    appweburl, hostweburl);

                // Send the request and return the response.
                return jQuery.ajax({
                    url: listItemAllFieldsEndpoint,
                    type: "GET",
                    headers: { "accept": "application/json;odata=verbose" }
                });
            }
        }

        dialog = $("#afbeeldingDialog").dialog({
            autoOpen: false,
            height: 400,
            width: 600,
            modal: true,
            buttons: {
                "Voeg afbeeling toe": uploadBijlage,
                Cancel: function () {
                    dialog.dialog("close");
                    editor.focus();
                }
            },
            close: function () {
                dialog.dialog("close");
                editor.focus();
            }
        });

        dialog.dialog("open");
    }

    ns.getAfbeeldingDialogForm = function () {
        var div = $("<div />")
            .attr("id", "afbeeldingDialog");
        var omschrijving = $("<p>Kies de afbeelding die je wilt invoegen in de Tickets bijlage bibliotheek</p>");
        var input = $("<input />")
            .attr("id", "getAfbeelding")
            .attr("type", "file")
            .attr("accept", "image/*");
        var submit = $("<input />")
            .attr("id", "afbeeldingSubmit")
            .attr("type", "submit")
            .attr("tabindex", "-1")
            .attr("style", "position:absolute; top:-1000px");
        div.append(omschrijving);
        div.append(input);
        div.append(submit);
        return div;
    }

    return ns;

})(be.itc.sp.editor.tinymce || {});