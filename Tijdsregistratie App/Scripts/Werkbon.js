﻿'use strict';

$(document).ready(function () {
    var id = GetURLParameter('ID');
    GetTijdsregistratieByIdViewModel(id);
});

function PrintBon() {
    //alert("print");
    var printbutton = document.getElementById("printbutton");
    printbutton.style.display = "none";
    window.print();
    printbutton.style.display = "block";
}

function GetJsonAndFillForm() {
    $("#ticketnummerLabel").html("5630");
    $("#AdresLabel").html("rechterstraat 114<br/>3511 Kuringen");
    $("#DatumLabel").html("10/06/2014");
    $("#startuur").html("11:00:00");
    $("#Einduur").html("12:00:00");
    $("#EngeneerLabel").html("Stefan Kelchtermans");
    $("#FoutMeldingLabel").html("Hosting");
    $("#opmerkingArea").val("Test");//.html("test\nopm");
}