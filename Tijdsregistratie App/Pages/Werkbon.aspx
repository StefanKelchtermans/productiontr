﻿<%@ Page language="C#" MasterPageFile="~masterurl/default.master" Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<asp:Content ContentPlaceHolderId="PlaceHolderAdditionalPageHead" runat="server">
    <SharePoint:ScriptLink name="sp.js" runat="server" OnDemand="true" LoadAfterUI="true" Localizable="false" />
    <script type="text/javascript" src="../Scripts/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.runtime.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.js"></script>
    <script type="text/javascript" src="../Scripts/DatumFuncties.js"></script>
    <script type="text/javascript" src="../Scripts/Models/GetTijdsregisratieByIdViewModel.js"></script>
    <script type="text/javascript" src="../Scripts/Werkbon.js"></script>

    <style type="text/css">
        body {
            font-size: 11px;
        }
        .container {
            width: 650px;
            border:1px solid black;
            background-color: white;
        }

        #logo {
            margin-left:290px;
        }
        .gegevens {
            width:205px; 
            height:170px; 
            float:left;
            margin:0;
            padding:5px;
            border-left: 1px solid black;
        }

        #onsGegevens {
            border-left: none;
        }

        .BoldParagraph {
            font-weight: 600;
        }

        #VerslagDiv {
            clear: left;
            background-color: gray;
            color: white;
            text-align: center;
            border-top: 1px solid black;
            border-bottom:1px solid black;
        }

            #VerslagDiv h3 {
                margin: 0;
                padding: 3px;
            }

        .InterventieGegevens {
            border-left: 1px solid black;
            width: 205px;
            padding:0px 5px;
            height: 30px;
            float: left;
        }

        #leftdiv {
            border-left: none;
        }

        .gewoon {
            clear: left;
            border-top: 1px solid black;
            height:60px;
            padding-left: 5px;
        }

        .AkkoordDiv {
            height: 70px;
            border-top: 1px solid black;
            padding-left: 5px;
        }

        .OpmerkingDiv {
            height: 380px;
            border-top: 1px solid black;
            padding-left: 5px;
        }

        .invoerveld {
            border: none;
            overflow:hidden;
            margin:0;
        }

            .invoerveld:hover {
                border: 1px solid #d5d4d4;
            }

        input[type='text'] {
            border: none;
            overflow: hidden;
            margin: 0;
        }

            input[type='text']:hover {
                border: 1px solid #d5d4d4;
            }

        .underline {
            text-decoration: underline;
            padding-top:2px;
            padding-left:5px;
        }

        .dot {
            padding: 7px 10px 0 10px;
        }
        table {
            border: none;
            border-spacing:0;
            padding:0 0 0 2px;
            font-size:11px;
        }

        #opmerkingTextBox {
            width:96%;
            height:85%;
        }

    </style>
</asp:Content>

<asp:Content ContentPlaceHolderId="PlaceHolderMain" runat="server">
    <%--<WebPartPages:WebPartZone runat="server" FrameType="TitleBarOnly" ID="full" Title="loc:full" />--%>
    <img id="logo"alt="ITC Logo" src="../Images/itclogosmall.jpg" />
    <div class="container">
        <div id="onsGegevens" class="gegevens">
            <p class="BoldParagraph">Rechterstraat 114, 3500 Hasselt</p>
            <table>
                <tbody>
                    <tr>
                        <td>tel:</td>
                        <td>011/28.63.20</td>
                    </tr>
                    <tr>
                        <td>Fax:</td>
                        <td>011/28.63.29</td>
                    </tr>
                    <tr>
                        <td>E-mail:</td>
                        <td>info@itc.be</td>
                    </tr>
                    <tr>
                        <td>BTW nr:</td>
                        <td>BE 0871.668.922</td>
                    </tr>
                    <tr>
                        <td>RPR:</td>
                        <td>HASSELT</td>
                    </tr>
                    <tr>
                        <td>Bank:</td>
                        <td>ING 330-0259418-94</td>
                    </tr>
                    <tr>
                        <td>Iban:</td>
                        <td>BE87 330 02594 1894</td>
                    </tr>
                    <tr>
                        <td>Bic:</td>
                        <td>BBRUBEBB</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="gegevens">
            <p class="BoldParagraph">Referentie nummer:</p>
            <p>Ticket <input type="text" id="ticketnummerTextBox" data-bind="text: Registratie().ticketnr" class="invoerveld" /></p>
        </div>
        <div class="gegevens">
            <p class="BoldParagraph">Interventie adres: </p>
            <textarea id="AdresTextBox" class="invoerveld" cols="30" rows="8"></textarea>
        </div>
        <div id="VerslagDiv">
            <h3>INTERVENTIE VERSLAG</h3>
        </div>
        <div id="leftdiv" class="InterventieGegevens">
            <p><span id="Label1" class="underline">DATUM:</span>&nbsp;<input type="text" id="DatumTextBox" class="invoerveld" /></p>
        </div>
        <div class="InterventieGegevens">
            <p><span class="underline">VAN:</span>&nbsp;<input type="text" id="startuurTextBox" class="invoerveld" /></p>
        </div>
        <div class="InterventieGegevens">
            <p><span class="underline">TOT:</span>&nbsp;<input type="text" id="EinduurTextBox" class="invoerveld" /></p>
        </div>
        <div class="gewoon">
            <p class="underline">INITIALEN ENGINEER(S): </p>
<%--            <asp:Label ID="EngeneerLabel" runat="server"></asp:Label>--%>
            <input type="text" id="EngeneerTextBox" runat="server" style="width:96%;"  class="invoerveld" />
        </div>
        <div class="gewoon">
            <p class="underline">FOUTMELDING/OMSCHRIJVING INTERVENTIE:</p>
<%--            <asp:Label ID="FoutMeldingLabel" runat="server"></asp:Label>--%>
            <input type="text" id="FoutmeldingTextBox" style="width:98%;" class="invoerveld" />
        </div>
        <div class="OpmerkingDiv">
            <p class="underline">OPMERKING:</p>
            <textarea id="opmerkingArea" class="invoerveld" cols="105" rows="20" ></textarea>
        </div>
        <div class="AkkoordDiv">
            <p class="underline">VOOR AKKOORD:</p>
            <p>NAAM:</p>
        </div>

        <input id="printbutton" type="button" value="print" title="print" onclick="PrintBon();" />
    </div>
    <div>
        <asp:Label ID="errorLabel" runat="server"></asp:Label>
    </div>
</asp:Content>
