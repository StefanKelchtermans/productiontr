﻿<%-- The following 4 lines are ASP.NET directives needed when using SharePoint components --%>

<%@ Page Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" MasterPageFile="~masterurl/default.master" Language="C#" %>

<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%-- The markup and script in the following Content element will be placed in the <head> of the page --%>
<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">

    <!-- Add your CSS styles to the following file -->
    <link rel="Stylesheet" type="text/css" href="../Content/App.css" />
    <link rel="Stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" />

    <!-- Add your JavaScript to the following file -->
    <script src="../Scripts/jquery-1.9.1.min.js"></script>
    <script src="/_layouts/15/sp.runtime.js"></script>
    <script src="/_layouts/15/sp.js"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/knockout/knockout-2.2.1.js"></script>
    <script src="../Scripts/Models/siteConfigs.js"></script>
    <script src="../Scripts/jquery.dataTables.min.js"></script>
    <script src="../Scripts/jquery.tablesorter.min.js"></script>
    <script src="../Scripts/jquery.tablesorter.widgets.min.js"></script>
    <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script src="../Scripts/Models/FilterSetting.js"></script>
    <script src="../Scripts/datepicker-nl.js"></script>
    <script src="../Scripts/tinymce/js/tinymce_4.1.9_jquery/tinymce/js/tinymce/tinymce.min.js"></script>
    <script src="../Scripts/Models/RendabelViewModel.js"></script>
    <script src="../Scripts/DatumFuncties.js"></script>
    <script src="../Scripts/Models/OnsiteCheckModel.js"></script>
    <script src="../Scripts/Models/DrempelwaardeViewModel.js"></script>
    <script src="../Scripts/Models/StatusViewModel.js"></script>
    <script src="../Scripts/Models/UsersViewModel.js"></script>
    <script src="../Scripts/Models/MijnTicketsViewModel.js"></script>
    <script src="../Scripts/Models/TijdsregistratiesViewModel.js"></script>
    <script src="../Scripts/Models/LastRegistratieModel.js"></script>
    <script src="../Scripts/Models/KlantTicketsViewModel.js"></script>
    <script src="../Scripts/Models/HelpdeskTicketViewModel.js"></script>
    <script src="../Scripts/Models/OpenstaandeTicketsViewModel.js"></script>
    <script src="../Scripts/Models/KlantAankondigingViewModel.js"></script>
    <script src="../Scripts/Models/Registratie.js"></script>
    <script src="../Scripts/Models/Ticket.js"></script>
    <script src="../Scripts/Map.js"></script>
    <script src="../Scripts/upload.js"></script>
    <script src="../Scripts/be.itc.sp.exception.js"></script>
    <script src="../Scripts/be.itc.sp.rest.fileupload.js"></script>
    <script src="../Scripts/be.itc.sp.editor.tinymce.js"></script>
    <script src="../Scripts/App.js"></script>
    <script type="text/javascript">
        function allowDrop(ev) {
            ev.preventDefault();
        }

        function drag(ev) {
            ev.dataTransfer.setData("text/html", ev.target.id);
        }

        function drop(ev) {
            ev.preventDefault();
            var data = ev.dataTransfer.getData("text/html");
            ev.target.appendChild(document.getElementById(data));
        }

    </script>
</asp:Content>

<%-- The markup in the following Content element will be placed in the TitleArea of the page --%>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    Tijdsregistraties
</asp:Content>

<%-- The markup and script in the following Content element will be placed in the <body> of the page --%>
<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <!--    <div ondrop="drop(event)" ondragover="allowDrop(event)" style="border: 1px solid black; width:100px; height:100px;">

    </div>
    <div ondrop="drop(event)" ondragover="allowDrop(event)" style="border: 1px solid black; width:100px; height:100px;">
        <img id="drag1" src="../Images/itclogosmall.jpg" draggable="true" ondragstart="drag(event)" alt="logo" />
    </div>-->
    <div>
        <!-- The following content will be replaced with the user name when you run the app - see App.js -->
        <p id="message">
            initializing...
        </p>
        <a id="top"></a>
    </div>
    <h3 class="hidden">Uren registratie</h3>
    <div class="centerContent stap hidden">
        <div class="redabelDiv">
            <span id="rd1">1</span>
        </div>
        <div class="redabelDiv">
            <span id="rd2">2</span>
        </div>
        <div class="redabelDiv">
            <span id="rd3">3</span>
        </div>
        <div class="redabelDiv">
            <span id="rd4">4</span>
        </div>
        <div class="redabelDiv">
            <span id="rd5">5</span>
        </div>
        <div class="redabelDiv">
            <span id="rd6">6</span>
        </div>
        <div class="redabelDiv">
            <span id="rd7">7</span>
        </div>
        <div class="redabelDiv">
            <span id="rd8">8</span>
        </div>
        <div class="redabelDiv">
            <span id="rd9">9</span>
        </div>
        <div class="redabelDiv">
            <span id="rd10">10</span>
        </div>
        <div class="redabelDiv">
            <span id="rd11">11</span>
        </div>
        <div class="redabelDiv">
            <span id="rd12">12</span>
        </div>
        <div class="redabelDiv">
            <span id="rd13">13</span>
        </div>
        <div class="redabelDiv">
            <span id="rd14">14</span>
        </div>
        <div class="redabelDiv">
            <span id="rd15">15</span>
        </div>
        <div class="redabelDiv">
            <span id="rd16">16</span>
        </div>
        <div class="redabelDiv">
            <span id="rd17">17</span>
        </div>
        <div class="redabelDiv">
            <span id="rd18">18</span>
        </div>
        <div class="redabelDiv">
            <span id="rd19">19</span>
        </div>
        <div class="redabelDiv">
            <span id="rd20">20</span>
        </div>
        <div class="redabelDiv">
            <span id="rd21">21</span>
        </div>
        <div class="redabelDiv">
            <span id="rd22">22</span>
        </div>
        <div class="redabelDiv">
            <span id="rd23">23</span>
        </div>
        <div class="redabelDiv">
            <span id="rd24">24</span>
        </div>
        <div class="redabelDiv">
            <span id="rd25">25</span>
        </div>
        <div class="redabelDiv">
            <span id="rd26">26</span>
        </div>
        <div class="redabelDiv">
            <span id="rd27">27</span>
        </div>
        <div class="redabelDiv">
            <span id="rd28">28</span>
        </div>
        <div class="redabelDiv">
            <span id="rd29">29</span>
        </div>
        <div class="redabelDiv">
            <span id="rd30">30</span>
        </div>
        <div class="redabelDiv">
            <span id="rd31">31</span>
        </div>
        <%--        <div class="clear" id="buttondiv">
            <a href="#" id="rendabelReloadButton" class="Buttons" onclick="ReloadRendabel();">Update</a>
        </div>--%>
    </div>

    <div id="overlaydiv"></div>
    <div id="TijdsegistratieDiv">
        <img src='../Images/close.gif' id="registratieCloseButton" class="closeButton hidden" onclick="RemovePopup();" alt='Sluit popup' />

        <h3>Tijdsregistraties</h3>
        <div class="stap" style="overflow-y: auto; height: 94%;">
            <nav>
                <ul>
                    <li>Speciale tickets
                        <ul id="speciaal">
                            <li>1 Verplaatsing</li>
                            <li>2 Verlof</li>
                            <li>3 Recup</li>
                            <li>4 Ziek</li>
                            <li>5 Opleiding/examen</li>
                            <li>6 Vergadering ITC Belgium</li>
                            <li>7 Belgacom intervantie</li>
                            <li>8 Permanentie ticket</li>
                            <li>9 ITC Belgium intern werk</li>
                            <li>10 Planninig en organisatie technische dienst</li>
                            <li>11 Dummy</li>
                            <li>15 Controle bachup en Kaseya </li>
                            <li>20 Registratie oud ticket</li>
                        </ul>
                    </li>
                    <li id="closePopUp" class="hidden"><a href="#" onclick="RemovePopup();">Sluiten</a></li>
                </ul>
            </nav>
            <div class="samenOpslaanDiv">
                <div class="blok1">
                    <table>
                        <tbody>
                            <tr>
                                <td colspan="5">
                                    <span id="KlantGegevens" class="bluetext"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Ticket nummer</td>
                                <td>
                                    <input type="text" id="ticketNummerTextBox" readonly="readonly" /></td>
                                <td><span id="nummerValidationSigne">*</span></td>
                            </tr>
                            <tr>
                                <td>Startuur</td>
                                <td>
                                    <input type="text" class="datepicker" id="startDatumTextBox" /></td>
                                <td>
                                    <select id="startuurUurSelect">
                                        <option value="07">07</option>
                                        <option value="08">08</option>
                                        <option value="09">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="00">00</option>
                                        <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option>
                                        <option value="05">05</option>
                                        <option value="06">06</option>
                                    </select>
                                </td>
                                <td>
                                    <select id="startuurMinuutSelect">
                                        <option value="00">00</option>
                                        <option value="05">05</option>
                                        <option value="10">10</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                        <option value="25">25</option>
                                        <option value="30">30</option>
                                        <option value="35">35</option>
                                        <option value="40">40</option>
                                        <option value="45">45</option>
                                        <option value="50">50</option>
                                        <option value="55">55</option>
                                    </select>
                                </td>
                                <td><span id="beginuurValidationSigne">*</span></td>
                            </tr>
                            <tr>
                                <td>Einduur</td>
                                <td></td>
                                <td>
                                    <select id="einduurUurSelect">
                                        <option value="07">07</option>
                                        <option value="08">08</option>
                                        <option value="09">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="00">00</option>
                                        <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option>
                                        <option value="05">05</option>
                                        <option value="06">06</option>
                                    </select>
                                </td>
                                <td>
                                    <select id="einduurMinuutSelect">
                                        <option value="00">00</option>
                                        <option value="05">05</option>
                                        <option value="10">10</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                        <option value="25">25</option>
                                        <option value="30">30</option>
                                        <option value="35">35</option>
                                        <option value="40">40</option>
                                        <option value="45">45</option>
                                        <option value="50">50</option>
                                        <option value="55">55</option>
                                    </select>
                                </td>
                                <td><span id="einduurValidationSigne">*</span></td>
                            </tr>
                            <tr>
                                <td><label for="facturatieTextBox">Facturatie tekst</label></td>
                                <td colspan="4">
                                    <textarea id="facturatieTextBox" cols="50" rows="8"></textarea></td>
                                <td><span id="facturatieValidationSigne">*</span></td>
                            </tr>
                            <tr>
                                <td>Onsite</td>
                                <td>
                                    <input type="radio" id="onsiteJaRadio" class="onsite" name="OnsiteRadios" value="Ja" />
                                    <label for="onsiteJaRadio">Ja</label>                                 
                                    <br />
                                    <input type="radio" id="onsiteNeeRadio" class="onsite" name="OnsiteRadios" value="Nee" />
                                    <label for="onsiteNeeRadio">Nee</label>
                                </td>
                                <td><span id="OnsiteValidationSpan">*</span></td>
                            </tr>
                            <tr>
                                <td>Permanentie</td>
                                <td>
                                    <input type="radio" id="permanentieJaRadio" class="permanentie" name="PermanentieRadios" value="Ja" />
                                    <label for="permanentieJaRadio">Ja</label><br />
                                    <input type="radio" id="permanentieNeeRadio" class="permanentie" name="PermanentieRadios" checked="checked" value="Nee" />
                                    <label for="permanentieNeeRadio">Nee</label>
                                </td>
                                <td><span id="permanentieValidationSpan">*</span></td>
                            </tr>
                            <tr>
                                <td><label for="klantenDocSelect">Klantendocumentatie aangevuld</label></td>
                                <td>
                                    <select id="klantenDocSelect">
                                        <option value="0"></option>
                                        <option value="N.v.t">N.v.t</option>
                                        <option value="Ja">Ja</option>
                                        <option value="Nee">Nee</option>
                                    </select>
                                </td>
                                <td><span id="klantenDocValidationSpan">*</span></td>
                            </tr>
                            <tr>
                                <td><label for="typeWerkSelect">Type werk</label></td>
                                <td>
                                    <select id="typeWerkSelect">
                                        <option value="Werkuren">Werkuren</option>
                                        <option value="Helpdesk">Helpdesk</option>
                                        <option value="Garantie">Garantie</option>
                                        <option value="Hosting">Hosting</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="projectSelect">Project</label></td>
                                <td colspan="2">
                                    <select id="projectSelect">
                                        <option value="Geen">Geen</option>
                                        <option value="A&B Partners SharePoint">A&B Partners SharePoint</option>
                                        <option value="Bioterra Stalencontroletool">Bioterra Stalencontroletool</option>
                                        <option value="CEO Ledenbeheer">CEO Ledenbeheer</option>
                                        <option value="Dummy">Dummy</option>
                                        <option value="Houben SharePoin">Houben SharePoint</option>
                                        <option value="Houthalen-Helchteren SharePoint">Houthalen-Helchteren SharePoint</option>
                                        <option value="ITC SharePoint">ITC SharePoint</option>
                                        <option value="ITC SharePoint - Administatie">ITC SharePoint - Administatie</option>
                                        <option value="ITC SharePoint Management">ITC SharePoint Management</option>
                                        <option value="ITC SharePoint sales">ITC SharePoint sales</option>
                                        <option value="ITC SharePoint software-afdeling">ITC SharePoint software-afdeling</option>
                                        <option value="ITC SharePoint technische dienst">ITC SharePoint technische dienst</option>
                                        <option value="ITC SharePoint Tijdsregistratie">ITC SharePoint Tijdsregistratie</option>
                                        <option value="Kastro website">Kastro website</option>
                                        <option value="Kerkstoel SharePoint">Kerkstoel SharePoint</option>
                                        <option value="MIC SharePoint EventBrite">MIC SharePoint EventBrite</option>
                                        <option value="P&R Medical Access SharePoint">P&R Medical Access SharePoint</option>
                                        <option value="Poms Planningstool">Poms Planningstool</option>
                                        <option value="Website ZVB">Website ZVB</option>
                                        <option value="ZVB SharePoint">ZVB SharePoint</option>
                                    </select>
                                    <br />
                                    <span class="bijTekst">Enkel voor software projecten.</span>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><a href="#" class="Buttons SaveButton" onclick="return RegistratieOpslaan(true);">Opslaan</a></td>
                            </tr>
                        </tbody>
                    </table>
                    <input type="hidden" id="FormklantidHidden" />
                    <div class="errordiv" id="formerrordiv">
                        <span class="error" id="formErrorLabel"></span>
                    </div>
                </div>

                <div class="blok2">
                    <table>
                        <tbody>
                            <tr>
                                <td>Ticket</td>
                                <td>
                                    <input type="text" id="ticketnrTextBox" readonly="readonly" />
                                    <span id="ticketNummerValedationSigne">*</span>

                                </td>
                                <td class="alignRight">
                                    <a href="#" id="copyFacturatieTekstButton" class="Buttons" onclick="return CopyFactText();">Plak</a>
                                </td>
                            </tr>
                            <tr>
                                <td>Uitgevoerde werken</td>
                                <td colspan="2">
                                    <textarea id="uitgevoerdeWerkenTextBox" class="editor"></textarea>
                                    <span id="uitgevoerdeValidationSigne">*</span></td>

                            </tr>
                            <tr>
                                <td />
                                <td>
                                    <div>
                                        <!--File Upload -->
                                        <b>Bijlagen</b>
                                        <br />
                                        <div id="attachmentList"></div>
                                        <div id="fileList"></div>
                                        <div id="addFileList"></div>
                                        <div id="fileInput"></div>
                                    </div>
                                </td>
                                <td />
                            </tr>
                            <tr>
                                <td><label for="toegewezenSelect">Toegewezen aan</label></td>
                                <td>
                                    <div id="toegewezenContainer">
                                        <div id="toegewezenLeft">
                                            <select id="toegewezenSelect" class="listbox" size="6" ondblclick="MoveToegewezenAanRight();">
                                            </select>
                                        </div>
                                        <div id="toegewezenMid">
                                            <a href="#" onclick="MoveToegewezenAanRight();" class="LeftRightButtons">&gt;&gt;</a><br />
                                            <br />
                                            <a href="#" onclick="MoveToegewezenAanLeft();" class="LeftRightButtons">&lt;&lt;</a>
                                        </div>
                                        <div id="toegewezenRight">
                                            <select id="selectedAsignedTo" class="listbox" size="6" ondblclick="MoveToegewezenAanLeft();">
                                            </select>
                                        </div>
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><label for="statusSelect">Status</label></td>
                                <td>
                                    <select id="statusSelect">
                                        <option value="Gepland">Gepland</option>
                                        <option value="Wachten op input klant">Wachten op input klant</option>
                                        <option value="Wachten op goederen">Wachten op goederen</option>
                                        <option value="Wachten op externen">Wachten op externen</option>
                                        <option value="Opvolging">Opvolging</option>
                                        <option value="Opvolging verlopen">Opvolging verlopen</option>
                                        <option value="Te plannen">Te plannen</option>
                                        <option value="Afgesloten">Afgesloten</option>
                                        <option value="Wacht op goedkeuring opdracht">Wacht op goedkeuring opdracht</option>
                                        <option value="In behandeling">In behandeling</option>
                                        <option value="Leveren">Leveren</option>
                                        <option value="Config">Config</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="prioriteitSelect">Prioriteit</label></td>
                                <td>
                                    <select id="prioriteitSelect">
                                        <option value="4">4</option>
                                        <option value="3">3</option>
                                        <option value="2">2</option>
                                        <option value="1">1</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="opvolgingsdatumTextBox">Opvolgingsdatum</label></td>
                                <td>
                                    <input type="text" class="datepicker" id="opvolgingsdatumTextBox" /></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td colspan="2"><!--<a href="#" id="ticketUpdateButton" class="Buttons SaveButton" onclick="return TicketOpslaanClick(true);">Opslaan</a>-->
                                    <a href="#" id="ticketUpdateButton" class="Buttons SaveButton" onclick="TicketOpslaanClick(true);">Opslaan</a>
                                    <a href="#" id="InbehandelingButton" class="Buttons" onclick="TicketInBehandelingButton_Click();">Plaats In behandeling</a></td>
                            </tr>
                        </tbody>
                    </table>
                    <div id="ticketErrorDiv" class="errordiv"></div>
                </div>
                <div style="clear: left; border-top: 1px solid #0171C5; padding: 1em; margin-top: 5px;">
                    <a href="#" class="Buttons  SaveButton" onclick="return SamenOpslaanClick();">Samen opslaan</a>
                </div>
            </div>
        </div>
    </div>
    <div class="registratieDiv">
        Tijdsregistraties van:
        <input type="text" class="datepicker" id="tijdsregistratieDatumTextBox" /><a href="#" class="Buttons" onclick="return UpdateTijdsregistraties();">Update</a>
        <div id="registraties">
            <table id="tijdsregistratiesTable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Ticket</th>
                        <th>Open</th>
                        <th>Startuur</th>
                        <th>Einduur</th>
                        <th>Duur</th>
                        <th>Facturatietekst</th>
                        <th>Klant</th>
                        <th>Onsite</th>
                    </tr>
                </thead>
                <tbody id="registratieTBody">
                </tbody>
            </table>

        </div>
        <div class="infodiv" id="totaalDuur">
            Totaal uren:
            <span id="totaalDuurLabel"></span>
        </div>
        <div class="infodiv">
            Totaal tussen 8 en 17 uur:
            <span id="totaalDagDuurLabel"></span>
        </div>
    </div>



    <h3>Mijn Tickets <a href="#" id="mijnTicketsLink" class="visibilityLink" onclick="return showHideMijnDiv();">Verberg</a> </h3>
    <div id="ticketDiv" class="stap">
        <div class="filterDiv">
            <table id="FilterTable">
                <tr>
                    <td>
                        <input type="checkbox" id="GeplandCheckBox" class="filterCheckBox" onclick="FilterOption(this.checked, 'Gepland');" checked="checked" value="Gepland" />Gepland</td>
                    <td>
                        <input type="checkbox" id="OpvolgCheckBox" class="filterCheckBox" onclick="FilterOption(this.checked, 'Opvolging');" value="Opvolging" />Opvolging</td>
                    <td>
                        <input type="checkbox" id="WachtCheckBox" class="filterCheckBox" onclick="FilterOption(this.checked, 'Wachten op input klant');" value="Wachten op input klant" />Wachten op input klant</td>
                    <td>
                        <input type="checkbox" id="WachtOpGoedCheckBox" class="filterCheckBox" onclick="FilterOption(this.checked, 'Wacht op goedkeuring opdracht klant');" value="Wacht op goedkeuring opdracht klant" />Wacht op goedkeuring opdracht klant</td>
                    <td>
                        <input type="checkbox" id="WachtOpGoederenCheckBox" class="filterCheckBox" onclick="FilterOption(this.checked, 'Wachten op goederen');" value="Wachten op goederen" />Wachten op goederen</td>
                    <td><a href="#" class="Buttons" onclick="return UpdateMyTickets(false);">Update</a></td>
                </tr>
                <tr>
                    <td>
                        <input type="checkbox" id="ConfigCheckBox" class="filterCheckBox" onclick="FilterOption(this.checked, 'Config');" value="Config" />Config</td>
                    <td>
                        <input type="checkbox" id="InBehandelingCheckBox" class="filterCheckBox" onclick="FilterOption(this.checked, 'In behandeling');" value="In behandeling" />In behandeling</td>
                    <td>
                        <input type="checkbox" id="OpvolgingVerlopenCheckBox" class="filterCheckBox" onclick="FilterOption(this.checked, 'Opvolging verlopen');" value="Opvolging verlopen" />Opvolging verlopen</td>
                    <td>
                        <input type="checkbox" id="TePlannenCheckBox" class="filterCheckBox" onclick="FilterOption(this.checked, 'Te plannen');" value="Te plannen" />Te plannen</td>
                    <td>
                        <input type="checkbox" id="LeverenCheckbox" class="filterCheckBox" onclick="FilterOption(this.checked, 'Leveren');" value="Leveren" />Leveren</td>
                    <td>
                        <input type="checkbox" id="WactenOpExternenCheckbox" class="filterCheckBox" onclick="FilterOption(this.checked, 'Wachten op externen');" value="Wachten op externen" />Wachten op externen</td>
                </tr>
                <tr>
                    <td>Filter op
                        <input id="nummerRadio" type="radio" name="zoek" onclick="return $('#txtzoek').focus();" />
                        nummer
                        <input id="klantRadio" type="radio" name="zoek" checked="checked" onclick="return $('#txtzoek').focus();" />
                        klant
                    </td>
                    <td>
                        <input type="text" id="txtzoek" onkeyup="return ZoekKeyUp();" /></td>
                    <td><a href="#" onclick="SaveFilterConfig();" class="Buttons">Filters opslaan </a></td>
                    <td><a href="#" onclick="OpenSortingDiv();" class="Buttons">Sorteer opties</a></td>
                </tr>
            </table>
        </div>
        <div id="ticketsDiv">
            <table id="ticketTabel" class="sortable">
                <thead>
                    <tr>
                        <th>Nummer
                            <img src="../Images/Sorteer.png" alt="*" width="8" /></th>
                        <th>Type ticket
                            <img src="../Images/Sorteer.png" alt="*" width="8" /></th>
                        <th>Open
                            <img src="../Images/Sorteer.png" alt="*" width="8" /></th>
                        <th>Korte omschrijving
                            <img src="../Images/Sorteer.png" alt="*" width="8" /></th>
                        <th>Werkgebied
                            <img src="../Images/Sorteer.png" alt="*" width="8" /></th>
                        <th>Prioriteit
                            <img src="../Images/Sorteer.png" alt="*" width="8" /></th>
                        <th>Status
                            <img src="../Images/Sorteer.png" alt="*" width="8" /></th>
                        <th>Gewijzigd door
                            <img src="../Images/Sorteer.png" alt="*" width="8" /></th>
                        <th>Laatst gewijzigd
                            <img src="../Images/Sorteer.png " alt="*" width="8" /></th>
                        <th>Klant
                            <img src="../Images/Sorteer.png" alt="*" width="8" /></th>
                        <th>Contact
                            <img src="../Images/Sorteer.png" alt="*" width="8" /></th>
                        <th>Opvoldatum
                            <img src="../Images/Sorteer.png" alt="*" width="8" /></th>
                    </tr>
                </thead>
                <tbody id="ticketTBody">
                    <%--                    <tr>
                        <td data-bind="text: ID"></td>
                        <td>
                            <a href="#" onclick="ShowTijdsregistratieDiv();" data-bind="text: Type"></a>
                        </td>
                        <td>
                            <a href="#" onclick="return OpenTicket();" >
                                <img src="../Images/pen_paper_2-512.png" alt="Open ticket" width="20" />
                            </a>
                        </td>
                        <td data-bind="text: Omschrijving"></td>
                        <td data-bind="text: Werkgebied"></td>
                        <td data-bind="text: Prioriteit"></td>
                        <td data-bind="text: Status"></td>
                        <td data-bind="text: GewijzigdDoor"></td>
                        <td data-bind="text: Gewijzigd"></td>
                        <td data-bind="text: Klant"></td>
                        <td data-bind="text: Contact"></td>
                        <td data-bind="text: Opvolgdatum"></td>
                        <td><input type="hidden" id="TG" data-bind="value: ToegewezenAan" /></td>
                    </tr>--%>
                </tbody>
            </table>

        </div>
        <div class="infodiv">
            Totaal tickets: <span id="TotaalTicketsLabel"></span>
        </div>
        <div class="errordiv" id="Div2">
            <span class="error" id="TicketErrorLabel"></span>
        </div>
    </div>
    <h3>Helpdesk tickets <a href="#" id="helpdeskTicketlink" class="visibilityLink" onclick="return showHideHelpdeskDiv();">Verberg</a></h3>
    <div id="HelpdeskDiv" class="stap">
        <div class="filterDiv">
            <table id="HelpdeskFilterTable">
                <tr>
                    <td>
                        <input type="checkbox" id="HelpdeskGeplandCheckbox" class="filterCheckBox" onclick="HelpdeskFilterOption(this.checked, 'Gepland');" checked="checked" value="Geplant" />Gepland</td>
                    <td>
                        <input type="checkbox" id="HelpeskOpvolgCheckbox" class="filterCheckBox" onclick="HelpdeskFilterOption(this.checked, 'Opvolging');" value="Opvolging" />Opvolging</td>
                    <td>
                        <input type="checkbox" id="HelpdeskWachtCheckbox" class="filterCheckBox" onclick="HelpdeskFilterOption(this.checked, 'Wachten op input klant');" value="Wachten op input klant" />Wachten op input klant</td>
                    <td>
                        <input type="checkbox" id="HelpdeskWachtGoedkCheckbox" class="filterCheckBox" onclick="HelpdeskFilterOption(this.checked, 'Wacht op goedkeuring opdracht klant');" value="Wacht op goedkeuring opdracht klant" />Wacht op goedkeuring opdracht klant</td>
                    <td>
                        <input type="checkbox" id="HelpdeskWachtGoedeCheckbox" class="filterCheckBox" onclick="HelpdeskFilterOption(this.checked, 'Wachten op goederen');" value="Wachten op goederen" />Wachten op goederen</td>
                    <td><a href="#" class="Buttons" onclick="return UpdateHDTickets(false);">Update</a></td>
                </tr>
                <tr>
                    <td>
                        <input type="checkbox" id="HelpdeskConfigCheckbox" class="filterCheckBox" onclick="HelpdeskFilterOption(this.checked, 'Config');" value="Config" />Config</td>
                    <td>
                        <input type="checkbox" id="HelpdeskBehandelCheckbox" class="filterCheckBox" onclick="HelpdeskFilterOption(this.checked, 'In behandeling');" value="In behandeling" />In behandeling</td>
                    <td>
                        <input type="checkbox" id="HelpdeskVerlopenCheckbox" class="filterCheckBox" onclick="HelpdeskFilterOption(this.checked, 'Opvolging verlopen');" value="Opvolging verlopen" />Opvolging verlopen</td>
                    <td>
                        <input type="checkbox" id="HelpdeskPlannenCheckbox" class="filterCheckBox" onclick="HelpdeskFilterOption(this.checked, 'Te plannen');" value="Te plannen" />Te plannen</td>
                    <td>
                        <input type="checkbox" id="HelpdeskLeverenCheckbox" class="filterCheckBox" onclick="HelpdeskFilterOption(this.checked, 'Leveren');" value="Leveren" />Leveren</td>
                    <td>
                        <input type="checkbox" id="HelpdeskWactenOpExternenCheckbox" class="filterCheckBox" onclick="HelpdeskFilterOption(this.checked, 'Wachten op externen');" value="Wachten op externen" />Wachten op externen</td>
                </tr>
                <tr>

                    <td>Filter op
                        <input id="HelpdeskNummerRadio" type="radio" name="zoekTD" onclick="return $('#HelpdeskZoekTextBox').focus();" />
                        nummer
                        <input id="HelpdeskKlantRadio" type="radio" name="zoekTD" checked="checked" onclick="return $('#HelpdeskZoekTextBox').focus();" />
                        klant
                    </td>
                    <td>
                        <input type="text" id="HelpdeskZoekTextBox" onkeyup="return HelpdeskZoekKeyUp();" /></td>
                    <td><a href="#" onclick="SaveHDFilterConfig();" class="Buttons">Filter opslaan</a></td>
                </tr>
            </table>
        </div>
        <div id="Div3">
            <table id="helpdeskTicketTable" class="sortable">
                <thead>
                    <tr>
                        <th>Nummer
                            <img src="../Images/Sorteer.png" alt="*" width="8" /></th>
                        <th>Type ticket
                            <img src="../Images/Sorteer.png" alt="*" width="8" /></th>
                        <th>Open
                            <img src="../Images/Sorteer.png" alt="*" width="8" /></th>
                        <th>Korte omschrijving
                            <img src="../Images/Sorteer.png" alt="*" width="8" /></th>
                        <th>Werkgebied
                            <img src="../Images/Sorteer.png" alt="*" width="8" /></th>
                        <th>Prioriteit
                            <img src="../Images/Sorteer.png" alt="*" width="8" /></th>
                        <th>Status
                            <img src="../Images/Sorteer.png" alt="*" width="8" /></th>
                        <th>Gewijzigd door
                            <img src="../Images/Sorteer.png" alt="*" width="8" /></th>
                        <th>Laatst gewijzigd
                            <img src="../Images/Sorteer.png " alt="*" width="8" /></th>
                        <th>Klant
                            <img src="../Images/Sorteer.png" alt="*" width="8" /></th>
                        <th>Contact
                            <img src="../Images/Sorteer.png" alt="*" width="8" /></th>
                        <th>Opvoldatum
                            <img src="../Images/Sorteer.png" alt="*" width="8" /></th>
                    </tr>
                </thead>
                <tbody id="HelpdeskTbody">
                    <%--                    <tr>
                        <td data-bind="text: ID"></td>
                        <td>
                            <a href="#" onclick="ShowTijdsregistratieDiv();" data-bind="text: Type"></a>
                        </td>
                        <td>
                            <a href="#" onclick="return OpenTicket();" >
                                <img src="../Images/pen_paper_2-512.png" alt="Open ticket" width="20" />
                            </a>
                        </td>
                        <td data-bind="text: Omschrijving"></td>
                        <td data-bind="text: Werkgebied"></td>
                        <td data-bind="text: Prioriteit"></td>
                        <td data-bind="text: Status"></td>
                        <td data-bind="text: GewijzigdDoor"></td>
                        <td data-bind="text: Gewijzigd"></td>
                        <td data-bind="text: Klant"></td>
                        <td data-bind="text: Contact"></td>
                        <td data-bind="text: Opvolgdatum"></td>
                        <td><input type="hidden" id="HelpdeskToegewezenAanHidden" data-bind="value: ToegewezenAan" /></td>
                    </tr>--%>
                </tbody>
            </table>

        </div>
        <div class="infodiv">
            Totaal tickets: <span id="TotaaHelpdeskSpan"></span>
        </div>
        <div class="errordiv" id="HelpdeskErrorDiv">
            <span class="error" id="HelpdeskErrorSpan"></span>
        </div>
    </div>
    <div id="KlantTicketsDiv">
        <img src='../Images/close.gif' class="closeButton hidden" onclick="CloseKlantPopUp();" alt='Sluit popup' />
        <h3>Openstaande tickets van
            <label id="klantLabel" />
        </h3>
        <div class="stap" style="overflow-y: auto; height: 94%;">
            <div id="klantDetailsDiv">
                <table id="klantdetailTable">
                    <tbody>
                        <tr>
                            <td>Bedrijf:</td>
                            <td>
                                <label id="bedrijfLabel" />
                            </td>
                            <td>Opmerking factuur:</td>
                            <td>Opmerking Klant:</td>
                        </tr>
                        <tr>
                            <td>Adres:</td>
                            <td>
                                <label id="adresLabel" />
                            </td>
                            <td id="OpmFactuurCell" rowspan="3"></td>
                            <td id="OpmKlantCell" rowspan="3"></td>
                        </tr>
                        <tr>
                            <td>Telefoon:</td>
                            <td>
                                <label id="telefoonLabel" />
                            </td>
                        </tr>
                        <tr>
                            <td>Fax:</td>
                            <td>
                                <label id="faxLabel" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div>
                <table id="klantTicketTable">
                    <thead>
                        <tr>
                            <th>Nummer</th>
                            <th>Type ticket</th>
                            <th>Open</th>
                            <th>Korte omschrijving</th>
                            <th>Werkgebied</th>
                            <th>Prioriteit</th>
                            <th>Status</th>
                            <th>Gewijzigd door</th>
                            <th>Laatst gewijzigd</th>
                            <th>Klant</th>
                            <th>Contact</th>
                            <th>Opvoldatum </th>
                        </tr>
                    </thead>
                    <tbody id="KlantTicketTbody">
                    </tbody>
                </table>
            </div>
            <div>
                <h3>Tijdsregistraties</h3>
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Ticket</th>
                            <th>Open</th>
                            <th>Startuur</th>
                            <th>Einduur</th>
                            <th>Duur</th>
                            <th>Door</th>
                            <th>Facturatietekst</th>
                        </tr>
                    </thead>
                    <tbody id="klantTijdsregistratiesTbody">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div id="AankondigingenDiv" class="AankondigingPopUp hidden">
        <img src="../Images/close.gif" alt="" class="closeButton" onclick="HideAankondigingPopup();" />
        <h3>Aankondiging</h3>
        <div id="aankondigingContainer" class="stap" style="overflow-y: auto; height: 85%;">
        </div>
    </div>
    <div id="voorkeursSorteerDiv">
        <img src="../Images/close.gif" alt="" class="closeButton" onclick="HideVorrkeurPopup();" />
        <h3>Voorkeur sortering</h3>
        <div>
            <h4>Mijn tickets</h4>
            <label for="MTKolomSelect" class="sortingLabel">Kolom</label>
            <select id="MTKolomSelect">
                <option>Nummer</option>
                <option>Type ticket</option>
                <option>Werkgebied</option>
                <option>Prioriteit</option>
                <option>Status</option>
                <option>laatst gewijzigd</option>
                <option>Klant</option>
                <option>Opvolgdatum</option>
            </select><br />
            <label for="MTVolgordeSelect" class="sortingLabel">Volgorde</label>
            <select id="MTVolgordeSelect">
                <option>Ascending</option>
                <option>Descending</option>
            </select>
        </div>
        <div>
            <h4>Helpdesk tickets</h4>
            <label for="HDKolomSelect" class="sortingLabel">Kolom</label>
            <select id="HDKolomSelect">
                <option>Nummer</option>
                <option>Type ticket</option>
                <option>Werkgebied</option>
                <option>Prioriteit</option>
                <option>Status</option>
                <option>laatst gewijzigd</option>
                <option>Klant</option>
                <option>Opvolgdatum</option>
            </select><br />
            <label for="HDVolgordeSelect" class="sortingLabel">Volgorde</label>
            <select id="HDVolgordeSelect">
                <option>Ascending</option>
                <option>Descending</option>
            </select><br />
        </div>
        <div class="componentDiv"><a href="#" onclick="SaveSorting();" class="Buttons">Opslaan</a></div>
    </div>
</asp:Content>